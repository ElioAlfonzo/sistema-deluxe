<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prendaempresa extends Model
{
    public $table = "prendaempresas"; 
    protected $fillable = ['id','nombre', 'descripcion', 'condicion']; 
}
