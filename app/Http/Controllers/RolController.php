<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Rol; //importamos nuestro modelo

class RolController extends Controller
{
    public function index(Request $request)
    {   
        if(!$request->ajax()) return redirect('/');
        
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if($buscar==''){ //si buscar esta vacia
            $roles=Rol::orderBy('id','desc')->paginate(10);
        }else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
            $roles = Rol::where($criterio, 'like' , '%' . $buscar . '%' )->orderby('id','desc')->paginate(5);
        }


        return [
            'pagination' => [
                'total'         => $roles->total(),//el total de pag
                'current_page'  => $roles->currentPage(), //pagina actual
                'per_page'      => $roles->perPage(), //los registros de pagina
                'last_page'     => $roles->lastPage(), // ultima pagina
                'from'          => $roles->firstItem(),//desde la pagina
                'to'            => $roles->lastItem()//hasta la pagina
            ],
            'roles' => $roles
        ];
        
    }

    public function selectRol(Request $request)
    {
        $roles = Rol::where('condicion', '=', '1')
        ->select('id','nombre')
        ->orderBy('nombre','asc')->get();

        return ['roles' => $roles];
    }
}
