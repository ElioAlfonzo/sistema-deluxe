<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\AuthenticatesUsers; esta la borramos

//importamos estas 2 clases
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    public function showLoginForm(){
        return view('auth.login');//con esto me muestra la vista
    }

    public function login(Request $request){ //con esta funcion evaluare los datos
        //llamamos a a funcion validateLogin
        $this->validateLogin($request);

        //aqui realizamos laevaluacion si existen o no, con la clase Auth  su metodo attempt que usuario q viene por request sea igual al de mi base de datos
        if (Auth::attempt(['usuario' => $request->usuario, 'password' => $request->password, 'condicion'=>1])){
            //si el usuario existe q me redirec a main
            return redirect()->route('main');
        }

        //encaso de error que regrese y me muestre los errores y en que campo los muestre
        //el mensaje esta en resource auth
        return back()
        ->withErrors(['usuario' => trans('auth.failed')])
        ->withInput(request(['usuario'])); //este metodo despues de aparecer el error vuelve el usuario a su input
    }

    
    protected function validateLogin(Request $request){
        //vaidamos q sean de tipo  requerido
        $this->validate($request,[
            'usuario' => 'required|string',
            'password' => 'required|string' 
        ]);

    }

    public function logout(Request $request){ //Cerrar Session
        Auth::logout();
        $request->session()->invalidate();
        return redirect('/');
    }

}
