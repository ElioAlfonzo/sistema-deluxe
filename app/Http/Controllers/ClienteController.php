<?php

namespace App\Http\Controllers;
use App\Persona;
use Illuminate\Support\Facades\DB; //para trabajar con transacciones
use Illuminate\Http\Request;


class ClienteController extends Controller
{
    public function index(Request $request)
    {   
        if(!$request->ajax()) return redirect('/');
        
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if($buscar==''){ 
            $personas=Persona::select('id','nombre','id_cod_persona','tipo','direccion','telefono','email')
            ->where('tipo', '=', '1')
            ->paginate(10);
        }else{
            $personas=Persona::select('id','nombre','id_cod_persona','tipo','direccion','telefono','email')
            ->where('tipo', '=', '1')
            ->where($criterio, 'like' , '%' . $buscar . '%' )
            ->orderby('id','desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'         => $personas->total(),//el total de pag
                'current_page'  => $personas->currentPage(), //pagina actual
                'per_page'      => $personas->perPage(), //los registros de pagina
                'last_page'     => $personas->lastPage(), // ultima pagina
                'from'          => $personas->firstItem(),//desde la pagina
                'to'            => $personas->lastItem()//hasta la pagina
            ],
            'personas' => $personas
        ];
        
    }

    public function selectCliente(Request $request){
        
        if(!$request->ajax()) return redirect('/');
        
        $filtro = $request->filtro;//guardamos el filtro q viene por request
        $clientes=Persona::select('id','nombre','id_cod_persona','tipo')
        ->where('nombre', 'like' , '%' . $filtro . '%' )
        ->where('tipo', '=', '1')
        ->orderby('id','asc')->get();

        return ['clientes' => $clientes];
        // where('nombre', 'like' , '%' . $filtro . '%' )
        // ->orwhere('id_cod_persona', 'like' , '%' . $filtro . '%' )
    }
    
    public function selectClienteEm(Request $request){
        
        if(!$request->ajax()) return redirect('/');
        
        $filtro = $request->filtro;//guardamos el filtro q viene por request
        $clientes=Persona::select('id','nombre','id_cod_persona','tipo')
        ->where('nombre', 'like' , '%' . $filtro . '%' )
        ->where('tipo', '=', '2')
        ->orderby('nombre','asc')->get();

        return ['clientes' => $clientes];
        // where('nombre', 'like' , '%' . $filtro . '%' )
        // ->orwhere('id_cod_persona', 'like' , '%' . $filtro . '%' )
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $persona = new Persona();//instacia de la clase persona
            $persona->id_cod_persona = $request->id_cod_persona;
            $persona->nombre = $request->nombre;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;
            $persona->tipo = $request->tipo;
            $persona->save();

    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $persona = Persona::findOrFail($request->id);
        $persona->id_cod_persona = $request->id_cod_persona;
        $persona->nombre = $request->nombre;
        $persona->direccion = $request->direccion;
        $persona->telefono = $request->telefono;
        $persona->email = $request->email;
        $persona->tipo = $request->tipo;
        $persona->save();
        
    }

    public function codigoscli(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $codigo = Persona::all();
        return [
            'codigo' => $codigo->last()
        ];
        
    }

    // Funciones de Clientes empresas
    public function indexEm(Request $request)
    {   
        if(!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if($buscar==''){ 
            $personas=Persona::select('id','nombre','id_cod_persona','tipo','direccion','telefono','email','prendas')
            ->where('tipo', '=', '2')
            ->paginate(10);
        }else{
            $personas=Persona::select('id','nombre','id_cod_persona','tipo','direccion','telefono','email','prendas')
            ->where('tipo', '=', '2')
            ->where($criterio, 'like' , '%' . $buscar . '%' )
            ->orderby('id','desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'         => $personas->total(),//el total de pag
                'current_page'  => $personas->currentPage(), //pagina actual
                'per_page'      => $personas->perPage(), //los registros de pagina
                'last_page'     => $personas->lastPage(), // ultima pagina
                'from'          => $personas->firstItem(),//desde la pagina
                'to'            => $personas->lastItem()//hasta la pagina
            ],
            'personas' => $personas
        ];
        
    }

    //esta funcion era para cambiar prendas en la tabla Clientes
    public function cambiarPren(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $persona = Persona::findOrFail($request->id);
        $persona->prendas = 0;
        $persona->save();
        
    }
}
