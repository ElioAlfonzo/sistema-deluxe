<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; //para la hora actual
use App\Ordentrabajo;
use App\Ordenprenda;
use App\Pagoorden;
use App\Ordentipotra;
use App\Historiaordene;
use App\Inpestado;
use App\Guiade;
use App\Guiaprenda;
use App\Resumentrabajo;
use App\Pren_precio;
use App\Prendaempresa;

class DespachoController extends Controller
{
    public function index(Request $request)
    {
         if(!$request->ajax()) return redirect('/');
        
         $buscar = $request->buscar;
         $criterio = $request->criterio;
 
         if($buscar==''){ 
            $trabajos = Guiade::join('personas','id_perempresa','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->select('guia_despachos.guia_des','guia_despachos.guia_ordent','guia_despachos.nom_perem','guia_despachos.fecha_hora','guia_despachos.fecha_entrega','guia_despachos.hora_entrega')
            ->where('personas.tipo', '=', '2')
            ->orderBy('guia_despachos.id','desc')->paginate(10);
            
         }
         
         else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
            $trabajos = Guiade::join('personas','id_perempresa','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->select('guia_despachos.guia_des','guia_despachos.guia_ordent','guia_despachos.nom_perem','guia_despachos.fecha_hora','guia_despachos.fecha_entrega','guia_despachos.hora_entrega')
            ->where('personas.tipo', '=', '2')
            ->where('guia_despachos.'.$criterio, 'like', '%'. $buscar . '%')
            ->orderBy('guia_despachos.id','desc')->paginate(10);
          }
 
 
         return [
             'pagination' => [
                 'total'         => $trabajos->total(),//el total de pag
                 'current_page'  => $trabajos->currentPage(), //pagina actual
                 'per_page'      => $trabajos->perPage(), //los registros de pagina
                 'last_page'     => $trabajos->lastPage(), // ultima pagina
                 'from'          => $trabajos->firstItem(),//desde la pagina
                 'to'            => $trabajos->lastItem()//hasta la pagina
             ],
             'trabajos' => $trabajos
            //  'autenticado' => $autenticado
         ];
    }

    public function indexCli(Request $request)
    {
         if(!$request->ajax()) return redirect('/');
        
         $buscar = $request->buscar;
         $criterio = $request->criterio;
 
         if($buscar==''){ 
            $trabajos = Guiade::join('personas','id_perempresa','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->select('guia_despachos.guia_des','guia_despachos.guia_ordent','guia_despachos.nom_perem','guia_despachos.fecha_hora','guia_despachos.fecha_entrega','guia_despachos.hora_entrega')
            ->where('personas.tipo', '=', '1')
            ->orderBy('guia_despachos.id','desc')->paginate(10);
            
         }
         
         else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
            $trabajos = Guiade::join('personas','id_perempresa','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->select('guia_despachos.guia_des','guia_despachos.guia_ordent','guia_despachos.nom_perem','guia_despachos.fecha_hora','guia_despachos.fecha_entrega','guia_despachos.hora_entrega')
            ->where('personas.tipo', '=', '1')
            ->where('guia_despachos.'.$criterio, 'like', '%'. $buscar . '%')
            ->orderBy('guia_despachos.id','desc')->paginate(10);
          }
 
 
         return [
             'pagination' => [
                 'total'         => $trabajos->total(),//el total de pag
                 'current_page'  => $trabajos->currentPage(), //pagina actual
                 'per_page'      => $trabajos->perPage(), //los registros de pagina
                 'last_page'     => $trabajos->lastPage(), // ultima pagina
                 'from'          => $trabajos->firstItem(),//desde la pagina
                 'to'            => $trabajos->lastItem()//hasta la pagina
             ],
             'trabajos' => $trabajos
            //  'autenticado' => $autenticado
         ];
    }


    public function codigosdes(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Guiade::all();
        
        return [
            'codigo' => $codigo->last()
        ];
        
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        
        try{
            DB::beginTransaction();
 
            $mytime= Carbon::now('America/Santiago');
 
            $trabajo = new Guiade();
            $trabajo->guia_des = $request->guia_des;
            $trabajo->idorden = $request->num_orden;
            $trabajo->guia_ordent = $request->num_orden;
            $trabajo->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $trabajo->id_perempresa = $request->id_persona;
            $trabajo->nom_perem = $request->cliente;
            $trabajo->observacion = $request->observacion;
            $trabajo->fecha_hora = $mytime->toDateTimeString();
            $trabajo->fecha_entrega = $request->fecha_entrega;
            $trabajo->hora_entrega = $request->hora_entrega;
            $trabajo->save();

            $detalles = $request->data;//Array de detalles
            
            //Recorro todos los elementos
            foreach($detalles as $ep=>$det)
            {
                $detalle = new Guiaprenda();
                $detalle->id_guiades = $trabajo->id;
                $detalle->guia_ordent = $request->num_orden;
                $detalle->guiades = $trabajo->guia_des;
                $detalle->idempresa = $trabajo->id_perempresa;
                $detalle->nombre = $det['prenda'];
                $detalle->idprenda = $det['idprendas'];
                $detalle->cantiprendas = $det['stockprendas'];
                $detalle->guia_precio = $det['precio'];
                $detalle->fecha = $request->fecha_entrega;
                $detalle->save();

                

            }
            // Este para insertar todas las prendas con valor correspondiente
            if($request->tipo == '2'){
                $detalles1 = $request->data;//Array de detalles
                foreach($detalles1 as $epx=>$dete){
                    $resumen = new Resumentrabajo();
                    $resumen->ordent = $trabajo->guia_des;
                    $resumen->idempresa = $trabajo->id_perempresa;
                    $resumen->empresa = $trabajo->nom_perem;
                    $resumen->guiades = $trabajo->guia_des;
                    $resumen->idprendas = $dete['idprendas'];
                    $resumen->nombre = $dete['prenda'];
                    $resumen->precio = $dete['precio'];
                    $resumen->cantidad = $dete['stockprendas'];
                    $resumen->fecha = $request->fecha_entrega;
                    $resumen->tipo = '2';
                    $resumen->save();
                }
            }
            
            // Este para insertar todas las prendas convalor 0
            if($request->tipo == '2'){
                $detalles1 = $request->data2;//Array de detalles

                foreach($detalles1 as $epx=>$dete){

                    $resumen = new Resumentrabajo();
                    $resumen->ordent = $trabajo->guia_des;
                    $resumen->idempresa = $trabajo->id_perempresa;
                    $resumen->empresa = $trabajo->nom_perem;
                    $resumen->guiades = $trabajo->guia_des;
                    $resumen->idprendas = $dete['id'];
                    $resumen->nombre = $dete['nombre'];
                    $resumen->precio = $dete['precio'];
                    $resumen->cantidad = 0;
                    $resumen->fecha = $request->fecha_entrega;
                    $resumen->tipo = '2';
                    $resumen->save();

                }
            }
            
            foreach($detalles as $ep=>$det){

                $orden = Ordenprenda::findOrFail($det['id']);
                // Convertir string a int
                $valor1 = (int) $det['stockprendas2'];
                $valor2 = (int) $det['stockprendas'];
                $valor3 = $valor1 - $valor2;
                $orden->stockprendas = $valor3;
                $orden->save();

            }


            DB::commit();
            // return [
            //     'id' => $request->num_orden
            // ];

        } catch (Exception $e){
            DB::rollBack();
        }
    }

    //Store para Empresas
    
    public function storeEmpresa(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
        try{
            DB::beginTransaction();
 
            $mytime= Carbon::now('America/Santiago');
 
            $trabajo = new Guiade();
            $trabajo->guia_des = $request->guia_des;
            $trabajo->idorden = $request->num_orden;
            $trabajo->guia_ordent = $request->num_orden;
            $trabajo->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $trabajo->id_perempresa = $request->id_persona;
            $trabajo->nom_perem = $request->cliente;
            $trabajo->observacion = $request->observacion;
            $trabajo->fecha_hora = $mytime->toDateTimeString();
            $trabajo->fecha_entrega = $request->fecha_entrega;
            $trabajo->save();

            $detalles = $request->data;//Array de detalles
            
            //Recorro todos los elementos
            foreach($detalles as $ep=>$det)
            {
                $detalle = new Guiaprenda();
                $detalle->id_guiades = $trabajo->id;
                $detalle->guia_ordent = $request->num_orden;
                $detalle->guiades = $trabajo->guia_des;
                $detalle->idprenda = $det['idprendas'];
                $detalle->cantiprendas = $det['stockprendas'];
                $detalle->fecha = $mytime->toDateTimeString();
                $detalle->save();

                $resumen = new Resumentrabajo();
                $resumen->ordent = $trabajo->guia_des;
                $resumen->idempresa = $trabajo->id_perempresa;
                $resumen->empresa = $trabajo->nom_perem;
                $resumen->idprendas = $det['idprenda'];
                $resumen->precio = $det['precio'];
                $resumen->cantidad = $det['cantidad'];
                $resumen->fecha = $mytime->toDateTimeString();;
                $resumen->tipo = '2';
                $resumen->save();

            }
            
            DB::commit();
            // return [
            //     'id' => $request->num_orden
            // ];

        } catch (Exception $e){
            DB::rollBack();
        }
    }

    // para ver los detalles de los despachos en Guiadespacho
    public function obtenerCabecera(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        
        $id = $request->id;
        
           $trabajo = Guiade::join('personas','id_perempresa','=','personas.id_cod_persona')
           ->select('personas.nombre as nombre','guia_despachos.guia_des','guia_despachos.guia_ordent','guia_despachos.fecha_entrega','guia_despachos.observacion')
           ->where('guia_des', '=', $id)
           ->orderBy('guia_des','desc')->take(1)->get();

            return [
                'trabajo' => $trabajo
            ];

    }

    public function obtenerCabeDetalles(Request $request)
    {
         if(!$request->ajax()) return redirect('/');
         
         $id = $request->id;
            $prendas = Guiaprenda::join('prendas','guia_prendas.idprenda','=','prendas.id')
            ->select('prendas.nombre as prenda','guia_prendas.cantiprendas')
            ->where('guia_prendas.guiades', '=', $id)
            ->orderBy('guia_prendas.id','desc')->get();
             return [
                 'prendas' => $prendas
             ];
    }

    public function obtenerCabeDetallesEmpresa(Request $request)
    {
         if(!$request->ajax()) return redirect('/');
         $id = $request->id;
            $prendas = Guiaprenda::join('prendaempresas','guia_prendas.idprenda','=','prendaempresas.id')
            ->select('prendaempresas.nombre as prenda','guia_prendas.cantiprendas')
            ->where('guia_prendas.guiades', '=', $id)
            ->orderBy('guia_prendas.id','desc')->get();
             return [
                 'prendas' => $prendas
             ];
    }

    public function obtenerDetalles(Request $request)
    {
         //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $id = $request->id;
        //  ->join('users','idusuario','=','users.id')
            $prendas = Ordenprenda::join('prendas','orden_prendas.idprendas','=','prendas.id')
            ->select('orden_prendas.id','orden_prendas.prenda_numor','orden_prendas.stockprendas','orden_prendas.stockprendas as stockprendas2','orden_prendas.cantiprendas','prendas.nombre as prenda', 'orden_prendas.idprendas','orden_prendas.precio')
            ->where('orden_prendas.stockprendas', '>', '0')
            ->where('orden_prendas.prenda_numor', '=', $id)
            ->orderBy('orden_prendas.id','desc')->get();
             return [
                 'prendas' => $prendas
             ];

    }

    public function obtenerDetallesEmpresa(Request $request)
    {
         //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $id = $request->id;
         $person = $request->person;
        //  ->join('users','idusuario','=','users.id')
            $prendas = Ordenprenda::join('prendaempresas','orden_prendas.idprendas','=','prendaempresas.id')
            ->select('orden_prendas.id','orden_prendas.prenda_numor','orden_prendas.stockprendas','orden_prendas.stockprendas as stockprendas2','orden_prendas.cantiprendas','prendaempresas.nombre as prenda', 'orden_prendas.idprendas','orden_prendas.precio')
            ->where('orden_prendas.stockprendas', '>', '0')
            ->where('orden_prendas.prenda_numor', '=', $id)
            ->orderBy('orden_prendas.id','desc')->get();

            // Para traer las prendas perteneciente a este cliente
            $pren=Pren_precio::join('prendaempresas','idprendas','=','prendaempresas.id')
            ->where('pren_precios.idempresa', '=', $person)
            ->where('pren_precios.condicion', '=', '1') 
            ->select('prendaempresas.id','prendaempresas.nombre','pren_precios.precio')->get(); //pagination con elequent


             return [
                 'prendas' => $prendas,
                 'pren' => $pren
             ];

    }

    public function update(Request $request)
    {
        try{
            DB::beginTransaction();

                $orden = Ordentrabajo::findOrFail($request->id);
                $orden->estadoinven = 0;
                $orden->estado = 0;
                $orden->save();

                $histo = Historiaordene::findOrFail($request->historia);
                $histo->idinpes = 6;
                $histo->save();

            DB::commit();

        } catch (Exception $e){
            DB::rollBack();
        }
            

    } 

    public function pdf(Request $request, $id)
    {

        $trabajo = Guiade::join('personas','id_perempresa','=','personas.id_cod_persona')
        ->join('users','idusuario','=','users.id')
        ->select('guia_despachos.nom_perem','guia_ordent','guia_despachos.fecha_hora','guia_despachos.fecha_entrega','personas.id_cod_persona','personas.nombre','personas.direccion','personas.telefono','personas.email','guia_despachos.observacion',
        'users.usuario','users.nombre as nomusu')
        ->where('guia_des', '=', $id)
        ->orderBy('guia_des','desc')->take(1)->get();

        $prendas = Guiaprenda::join('prendaempresas','guia_prendas.idprenda','=','prendaempresas.id')
        ->select('guia_prendas.cantiprendas','prendaempresas.nombre as prenda','guia_prendas.idprenda')
        ->where('guia_prendas.guiades', '=', $id)
        ->orderBy('guia_prendas.id','desc')->get();

        $numorden = Guiade::select('guia_des')->where('id', $id)->get();

        // $local = OrdenTrabajo::join('ubicaciones', 'orden_trabajos.idubicacion','=', 'ubicaciones.id')
        // ->select('ubicaciones.local')
        // ->where('orden_trabajos.num_orden', '=', $id)
        // ->get();

        $pdf = \PDF::loadView('pdf.despa',['trabajo'=>$trabajo,'prenda'=>$prendas,'despa'=>$numorden]);
        return $pdf->stream('Orden Despacho N°'.$numorden[0]->num_orden.'.pdf');
    }
}
