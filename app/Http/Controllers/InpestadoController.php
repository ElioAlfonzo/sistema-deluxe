<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inpestado;

class InpestadoController extends Controller
{

    public function index(Request $request)
    {
        // if(!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        $criterio = $request->criterio; //aqui va el nombre del campo de la tabla a buscar

        if($buscar==''){
            $inpestado=Inpestado::orderBy('id','desc')->paginate(10); //pagination con elequent
        }else{
            //donde el texto buscar puede estar en el inicio o final de nuestro campo criterio
            $inpestado=Inpestado::where($criterio, 'like' , '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }
        
        //Instancias del paginador  cada una de las que necesitemos
        return [
            'paginacion' => [
                'total' => $inpestado->total(),
                'pagina_actual' =>$inpestado->currentPage(), //pag actual
                'pag_mostrar' =>$inpestado->perPage(), ////numero de registros por pagina
                'pag_alfinal' => $inpestado->lastPage(), //a cuantas paginas del final
                'pag_actual' =>$inpestado->firstItem(), //pagina actualen la q se encuentra
                'pag_ultima' =>$inpestado->lastItem(), //ultma Pagina
            ],
            'inpestados' => $inpestado
        ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $inpestado = new Inpestado();
        $inpestado->nombre = $request->nombre;
        $inpestado->descripcion = $request->descripcion;
        $inpestado->condicion = '1';
        $inpestado->save();
    }

   
    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $inpestado = Inpestado::findOrFail($request->id); //la fk como esta en la bd
        $inpestado->nombre = $request->nombre;
        $inpestado->descripcion = $request->descripcion;
        $inpestado->condicion = '1';
        $inpestado->save();
    }

    public function desactivar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $inpestado = Inpestado::findOrFail($request->id);
        $inpestado->condicion = '0';
        $inpestado->save();

    }

    public function activar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $inpestado = Inpestado::findOrFail($request->id);
        $inpestado->condicion = '1';
        $inpestado->save();
    }
}
