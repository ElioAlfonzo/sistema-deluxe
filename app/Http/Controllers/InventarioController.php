<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; //para la hora actual
use App\Ordentrabajo;
use App\Ordenprenda;
use App\Pagoorden;
use App\Ordentipotra;
use App\Formapago;
use App\Mediopago;
use App\Estadopago_prenda;
use App\Estado_pago;
use App\Historiaordene;
use App\Ubicacion;
use App\Prenda;
use App\Prendaempresa;
use App\Guiaprenda;
use App\Resumentrabajo;
use App\Persona;
use App\Exports\InventariosExport;
use App\Exports\InventariosEmExport;


use Maatwebsite\Excel\Facades\Excel;

class InventarioController extends Controller
{
    public function index(Request $request)
    {
        //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $buscar = $request->buscar;
         $criterio = $request->criterio;
         $ini = $request->ini;
         $fin = $request->fin;

         if($criterio == 0){
            $inventarios = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados',
            'historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '1')
            ->where('historia_ordenes.idinpes', '<=', '5')
            ->where('orden_prendas.stockprendas', '>', '0')->get();
         }
         
         if($criterio == 0 & $ini != '' & $fin != ''){
            $inventarios = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '1')
            ->where('historia_ordenes.idinpes', '<=', '5')
            ->where('orden_prendas.stockprendas', '>', '0')
            ->whereBetween('historia_ordenes.entrega',[$ini, $fin])->get();
         }
         
         if($criterio != 0 & $ini == '' & $fin == ''){
            $inventarios = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '1')
            ->where('orden_prendas.stockprendas', '>', '0')
            ->where('historia_ordenes.idinpes', '=', $criterio)->get();
         }

         if($criterio != 0 & $ini != '' & $fin !=''){
            $inventarios = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '1')
            ->where('historia_ordenes.idinpes', '=', $criterio)
            ->where('orden_prendas.stockprendas', '>', '0')
            ->whereBetween('historia_ordenes.entrega',[$ini, $fin])->get();
         }

            $prendas = Prenda::all();
            $ubicaciones = Ubicacion::all();
            
    
         return [
             'inventarios' => $inventarios,
             'ubicaciones' => $ubicaciones,
             'prendas' => $prendas
         ];
    }

    public function indexEm(Request $request)
    {
        //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $buscar = $request->buscar;
         $criterio = $request->criterio;
         $ini = $request->ini;
         $fin = $request->fin;

         if($criterio == 0){
            $inventarios = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '2')
            ->where('historia_ordenes.idinpes', '<=', '5')
            ->where('orden_prendas.stockprendas', '>', '0')->get();
         }
         
         if($criterio == 0 & $ini != '' & $fin != ''){
            $inventarios = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '2')
            ->where('historia_ordenes.idinpes', '<=', '5')
            ->where('orden_prendas.stockprendas', '>', '0')
            ->whereBetween('historia_ordenes.entrega',[$ini, $fin])->get();
         }
         
         if($criterio != 0 & $ini == '' & $fin == ''){
            $inventarios = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '2')
            ->where('orden_prendas.stockprendas', '>', '0')
            ->where('historia_ordenes.idinpes', '=', $criterio)->get();
         }

         if($criterio != 0 & $ini != '' & $fin !=''){
            $inventarios = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '2')
            ->where('historia_ordenes.idinpes', '=', $criterio)
            ->where('orden_prendas.stockprendas', '>', '0')
            ->whereBetween('historia_ordenes.entrega',[$ini, $fin])->get();
         }

            $prendas = Prendaempresa::all();
            $ubicaciones = Ubicacion::all();
            
    
         return [
             'inventarios' => $inventarios,
             'ubicaciones' => $ubicaciones,
             'prendas' => $prendas
         ];
    }

    public function pdf(Request $request)
    {

        $prendas=Ordenprenda::join('orden_trabajos','orden_prendas.idordentra','=','orden_trabajos.num_orden')
        ->join('inpestados','orden_prendas.idinpes','=','inpestados.id')
        ->join('prendas','orden_prendas.idprendas','=','prendas.id')
        ->join('historia_ordenes','orden_prendas.prenda_numor','=','historia_ordenes.histo_numor')
        
        ->select('prendas.nombre as prenda','orden_prendas.idordentra','orden_prendas.cantiprendas','inpestados.nombre as estado','orden_trabajos.fecha_hora as fechayhora','orden_trabajos.fecha_entrega as fechaen')
        ->orderBy('orden_trabajos.id','desc')
        ->where('historia_ordenes.idinpes', '<=', '3')->get();

        //para traer la forma de pago
        
        $pdf = \PDF::loadView('pdf.inventario',['prendas'=>$prendas]);
        return $pdf->stream('Orden de trabajo N°'.'.pdf');
    }


    public function export(Request $request){
      // return Excel::download(new InventariosExport, 'inventarios.xlsx');
      $criterio = $request->criterio;
      $ini = $request->ini;
      $fin = $request->fin;
      return (new InventariosExport($criterio,$ini,$fin))->download('inventarios.xlsx');
   }

   public function exportEm(Request $request){
    // return Excel::download(new InventariosExport, 'inventarios.xlsx');
    $criterio = $request->criterio;
    $ini = $request->ini;
    $fin = $request->fin;
    return (new InventariosEmExport($criterio,$ini,$fin))->download('inventarios.xlsx');
 }

      //Revisar si dejo esta funcion o no
    public function pago(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $ini = $request->ini;
        $fin = $request->fin;

      //   if($criterio == 0){
           $ordenp = Ordenprenda::join('orden_trabajos','orden_prendas.idordentra','=','orden_trabajos.num_orden')    
           ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.fecha')
         //   ->where('orden_trabajos.idpersona', '=', $criterio)
           ->whereBetween('orden_prendas.fecha',[$ini, $fin])->get();

           $guiap = Guiaprenda::join('orden_trabajos','guia_prendas.guia_ordent','=','orden_trabajos.num_orden')
           ->select('guia_prendas.idprenda','guia_prendas.cantiprendas', 'guia_prendas.fecha','guia_prendas.guia_precio')
         //   ->where('orden_trabajos.idpersona', '=', $criterio)
           ->whereBetween('guia_prendas.fecha',[$ini, $fin])->get();
           //   join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
         //   ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
         //   ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
      //   }

         $prendas = Prendaempresa::join('pren_precios','prendaempresas.id','=','pren_precios.idprendas')
         ->select('prendaempresas.id','prendaempresas.nombre','pren_precios.precio')
         ->where('pren_precios.idempresa', '=', $criterio)->get();

      // $orden = Ordentrabajo::all();

      return [
         'prendas' => $prendas,
         'ordenp' => $ordenp,
         'guiap' => $guiap
     ];
    }
      //  

    public function resumen(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $ini = $request->ini;
        $fin = $request->fin;

        $empresa = Persona::where('id_cod_persona', '=', $criterio)
         ->select('nombre')->get();

         $totalfecha = Resumentrabajo::where('idempresa', '=', $criterio)
         ->where('tipo','=', '2')
         ->whereBetween('fecha',[$ini, $fin])
         ->groupBy('idprendas','precio') 
         ->select(DB::raw('SUM(cantidad) as cantidad'),'idprendas','precio')
         ->orderBy('idprendas','asc')->get();

         $totales = Resumentrabajo::where('idempresa', '=', $criterio)
         ->where('tipo','=', '2')
         ->whereBetween('fecha',[$ini, $fin])
         ->groupBy('idprendas','precio') 
         ->select(DB::raw ('SUM(cantidad) * precio as total'),'idprendas')
         ->orderBy('idprendas','asc')->get();

         $prueba = Resumentrabajo::where('idempresa', '=', $criterio)
         ->where('tipo','=', '2')
         ->whereBetween('fecha',[$ini, $fin])
         ->groupBy('fecha')
         ->select(DB::raw ('SUM(cantidad) as total' ),'fecha')
         ->orderBy('fecha','asc')->get();
         
          $sum = $prueba->sum('total');

          $toto = $totales->sum('total');
          $iva = $toto * 19/100;
          $totalfinal = $toto + $iva;

           $guiap = Resumentrabajo::where('idempresa', '=', $criterio)
           ->where('tipo','=', '2')
           ->whereBetween('fecha',[$ini, $fin])
           ->groupBy('idprendas','fecha','nombre','precio')  
           ->select(DB::raw('SUM(cantidad) as cantidad'),'idprendas','fecha','nombre','precio')
           ->orderBy('idprendas','asc')->get();

           $pren = Resumentrabajo::where('idempresa', '=', $criterio)
           ->where('tipo','=', '2')
           ->whereBetween('fecha',[$ini, $fin])
           ->groupBy('idprendas','nombre','precio')  
           ->select(DB::raw('SUM(cantidad) as cantidad'),'idprendas','nombre','precio')
           ->orderBy('idprendas','asc')->get();

           $fechas = Guiaprenda::where('idempresa', '=', $criterio)
           ->whereBetween('fecha',[$ini, $fin])
           ->groupBy('fecha')  
           ->select('fecha')
           ->orderBy('fecha','asc')->get();


           $pren2 = Resumentrabajo::where('idempresa', '=', $criterio)
           ->where('tipo','=', '2')
           ->whereBetween('fecha',[$ini, $fin])
           ->groupBy('fecha','idprendas','nombre','precio')  
           ->select(DB::raw('SUM(cantidad) * precio as cantidad'),'fecha','idprendas','nombre')
           ->orderBy('idprendas','asc')->get();


          
           

      // $orden = Ordentrabajo::all();

      return [
         'guiap' => $guiap,
         'pren' => $pren,
         'fechas' => $fechas,
         'totalfecha' => $totalfecha,
         'totales' =>$totales,
         'pren2' =>$pren2,
         'empresa' => $empresa,
         'prueba' => $prueba,
         'sum' => $sum,
         'iva' => $iva,
         'toto' => $toto,
         'totalfinal'=>$totalfinal
         
     ];
    }

    public function pdfResumen(Request $request)
    {
      $criterio = $request->id;
      $ini = $request->ini;
      $fin = $request->fin;
      $folio = $request->folio;
        
        $valor =  strtotime($ini);
        $mes = date("m", $valor);

         $totalfecha = Resumentrabajo::where('idempresa', '=', $criterio)
         ->where('tipo','=', '2')
         ->whereBetween('fecha',[$ini, $fin])
         ->groupBy('idprendas','precio') 
         ->select(DB::raw('SUM(cantidad) as cantidad'),'idprendas','precio')
         ->orderBy('idprendas','asc')->get();

         $totales = Resumentrabajo::where('idempresa', '=', $criterio)
         ->where('tipo','=', '2')
         ->whereBetween('fecha',[$ini, $fin])
         ->groupBy('idprendas','precio') 
         ->select(DB::raw ('SUM(cantidad) * precio as total' ),'idprendas')
         ->orderBy('idprendas','asc')->get();

           $toto = $totales->sum('total');
           $iva = $toto * 19/100;
           $totalfinal = $toto + $iva;

          $guiap = Resumentrabajo::where('idempresa', '=', $criterio)
           ->where('tipo','=', '2')
           ->whereBetween('fecha',[$ini, $fin])
           ->groupBy('idprendas','fecha','nombre','precio')  
           ->select(DB::raw('SUM(cantidad) as cantidad'),'idprendas','fecha','nombre','precio')
           ->orderBy('idprendas','asc')->get();

          $pren = Resumentrabajo::where('idempresa', '=', $criterio)
          ->where('tipo','=', '2')
          ->whereBetween('fecha',[$ini, $fin])
          ->groupBy('idprendas','nombre','precio')  
          ->select(DB::raw('SUM(cantidad) as cantidad'),'idprendas','nombre','precio')
          ->orderBy('idprendas','asc')->get();

          

          $pren2 = Resumentrabajo::where('idempresa', '=', $criterio)
           ->where('tipo','=', '2')
           ->whereBetween('fecha',[$ini, $fin])
           ->groupBy('fecha','idprendas','nombre','precio')  
           ->select(DB::raw('SUM(cantidad) * precio as cantidad'),'fecha','idprendas','nombre')
           ->orderBy('idprendas','asc')->get();


          // $fechas = Guiaprenda::where('idempresa', '=', $criterio)
          // ->whereBetween('fecha',[$ini, $fin])
          // ->groupBy('fecha')  
          // ->select('fecha')
          // ->orderBy('idprenda','asc')->get();

          $fechas = Guiaprenda::where('idempresa', '=', $criterio)
           ->whereBetween('fecha',[$ini, $fin])
           ->groupBy('fecha')  
           ->select('fecha')
           ->orderBy('fecha','asc')->get();

      // $orden = Ordentrabajo::all();
          $prueba = Resumentrabajo::where('idempresa', '=', $criterio)
          ->where('tipo','=', '2')
          ->whereBetween('fecha',[$ini, $fin])
          ->groupBy('fecha')
          ->select(DB::raw ('SUM(cantidad) as total' ),'fecha')
          ->orderBy('fecha','asc')->get();

          $sum = $prueba->sum('total');
          

          $empresa = Persona::where('id_cod_persona', '=', $criterio)
         ->select('nombre')->get();

         $mytime= Carbon::now('America/Santiago');
         $mytime->toDateString();
      
        $pdf = \PDF::loadView('pdf.resumen',['guiap'=>$guiap,'pren2'=>$pren2,'fechas'=>$fechas,
        'totalfecha'=>$totalfecha,'totales'=>$totales,'hoy'=>$mytime,'ini'=>$ini,'fin'=>$fin,'pren'=>$pren,'prueba'=>$prueba,'suma'=>$sum,
        'toto'=>$toto, 'empresa'=>$empresa,'iva'=>$iva,'totalfinal'=>$totalfinal, 'mes' => $mes, 'folio'=>$folio]);
        return $pdf->stream('EP desde '. $ini.' hasta ' . $fin.'.pdf');
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
        try{
            DB::beginTransaction();
 
            $mytime= Carbon::now('America/Santiago');
 
            $trabajo = new Estado_pago();
            $trabajo->folio = $request->folio;
            $trabajo->fecha1 = $request->ini;
            $trabajo->fecha2 = $request->fin;
            // $trabajo->status = 1; //Pago si o no
            $trabajo->fechaorigen = $mytime->toDateTimeString();
            $trabajo->idempresa = $request->idempresa;
            $trabajo->empresa = $request->empresa;
            $trabajo->neto = $request->neto;
            $trabajo->iva = $request->iva;
            $trabajo->total = $request->total;
            $trabajo->save();
            
            // $detalles = $request->data;//Array de detalles
            
            // //Recorro todos los elementos
            // foreach($detalles as $ep=>$det)
            // {
            //     $detalle = new Ordenprenda();
            //     $detalle->idordentra = $trabajo->id;
            //     $detalle->prenda_numor = $trabajo->num_orden;
            //     $detalle->idprendas = $det['idprenda'];
            //     $detalle->cantiprendas = $det['cantidad'];
            //     $detalle->stockprendas = $det['cantidad'];
            //     // $detalle->idtipotrabajo = '1';
            //     $detalle->precio = $det['precio'];
            //     $detalle->idinpes = '1';
            //     $detalle->fecha = $mytime->toDateTimeString();;
            //     $detalle->save();
                
            // }

 
            DB::commit();
            // return [
            //     'id' => $request->num_orden
            // ];

        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function desactivar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $prenda = Estado_pago::findOrFail($request->id);
        $prenda->status = '0';
        $prenda->save();

    }


    public function codigosresu(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Estado_pago::all();
        
        return [
            'codigo' => $codigo->last()
        ];
        
    }

    public function estados(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if($buscar==''){
          $estado = Estado_pago::where('status', '=', '1')->paginate(8);

        }else{
          $estado = Estado_pago::where('status', '=', '1')
          ->where('estado_pago.'.$criterio, 'like', '%'. $buscar . '%')
          
          ->paginate(10);

        }

        return [
          'pagination' => [
              'total'         => $estado->total(),//el total de pag
              'current_page'  => $estado->currentPage(), //pagina actual
              'per_page'      => $estado->perPage(), //los registros de pagina
              'last_page'     => $estado->lastPage(), // ultima pagina
              'from'          => $estado->firstItem(),//desde la pagina
              'to'            => $estado->lastItem()//hasta la pagina
          ],

          'estado' => $estado
         //  'autenticado' => $autenticado
      ];
        
    }

    public function verificar(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        
        $criterio = $request->criterio;
        $ini = $request->ini;
        $fin = $request->fin;
        
        $veri = Estado_pago::select('folio')
        ->where('fecha1', '=', $ini)
        ->where('fecha2', '=', $fin)
        ->get();

      return [
         'verificado' => $veri
     ];
    }
}





