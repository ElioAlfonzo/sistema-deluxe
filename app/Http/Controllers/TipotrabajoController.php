<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Tipotrabajo; //importando el modelo

class TipotrabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        //esto es para realizar el buscar y viene por el request
        $buscar = $request->buscar;
        $criterio = $request->criterio; //aqui va el nombre del campo de la tabla a buscar

        if($buscar==''){
            $tipotrabajo=Tipotrabajo::orderBy('id','desc')->paginate(10); //pagination con elequent
        }else{
            //donde el texto buscar puede estar en el inicio o final de nuestro campo criterio
            $tipotrabajo=Tipotrabajo::where($criterio, 'like' , '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }
        
        //Instancias del paginador  cada una de las que necesitemos
        return [
            'paginacion' => [
                'total' => $tipotrabajo->total(),
                'pagina_actual' =>$tipotrabajo->currentPage(), //pag actual
                'pag_mostrar' =>$tipotrabajo->perPage(), ////numero de registros por pagina
                'pag_alfinal' => $tipotrabajo->lastPage(), //a cuantas paginas del final
                'pag_actual' =>$tipotrabajo->firstItem(), //pagina actualen la q se encuentra
                'pag_ultima' =>$tipotrabajo->lastItem(), //ultma Pagina
            ],
            'tipotrabajos' => $tipotrabajo
        ];
        
    }

    public function listarTipotrabajo(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        //esto es para realizar el buscar y viene por el request
        $buscar = $request->buscar;
        $criterio = $request->criterio; //aqui va el nombre del campo de la tabla a buscar

        if($buscar==''){
            $trabajo=Tipotrabajo::orderBy('nombre_tra','asc')->paginate(10); //pagination con elequent
        }else{
            //donde el texto buscar puede estar en el inicio o final de nuestro campo criterio
            $trabajo=Tipotrabajo::where($criterio, 'like', '%'. $buscar . '%')->orderBy('nombre_tra', 'asc')->paginate(10);
        }

        return [ 
                'pagination' => [
                'total'         => $trabajo->total(),//el total de pag
                'current_page'  => $trabajo->currentPage(), //pagina actual
                'per_page'      => $trabajo->perPage(), //los registros de pagina
                'last_page'     => $trabajo->lastPage(), // ultima pagina
                'from'          => $trabajo->firstItem(),//desde la pagina
                'to'            => $trabajo->lastItem()//hasta la pagina
            ],

            'trabajos' => $trabajo ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $tipotrabajo = new Tipotrabajo();
        $tipotrabajo->nombre_tra = $request->nombre;
        $tipotrabajo->descripcion = $request->descripcion;
        $tipotrabajo->condicion = '1';
        $tipotrabajo->save();
    }

   
    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $tipotrabajo = Tipotrabajo::findOrFail($request->id);
        $tipotrabajo->nombre_tra = $request->nombre;
        $tipotrabajo->descripcion = $request->descripcion;
        $tipotrabajo->condicion = '1';
        $tipotrabajo->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function desactivar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $tipotrabajo = TipoTrabajo::findOrFail($request->id);
        $tipotrabajo->condicion = '0';
        $tipotrabajo->save();

    }

    public function activar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $tipotrabajo = TipoTrabajo::findOrFail($request->id);
        $tipotrabajo->condicion = '1';
        $tipotrabajo->save();

    }
}
