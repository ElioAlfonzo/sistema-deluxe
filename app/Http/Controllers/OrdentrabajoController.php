<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; //para la hora actual
use App\Ordenprenda;
use App\Pagoorden;
use App\Ordentipotra;
use App\Formapago;
use App\Mediopago;
use App\Historiaordene;
use App\Inpestado;
use App\Persona;
use App\Ordentrabajo;
use App\Resumentrabajo;
use App\Prendaempresa; //importando el modelo
class OrdentrabajoController extends Controller
{
    public function index(Request $request)
    {
        // $autenticado =  Auth::user()->id_rol;
        //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
        if(\Auth::user()->id_rol == 1){
            $buscar = $request->buscar;
            $criterio = $request->criterio;
            if($buscar==''){ 
                $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
                ->join('users','idusuario','=','users.id')
                ->join('pago_ordenes','orden_trabajos.id','=','pago_ordenes.idordentrabajo')
                ->join('historia_ordenes', 'orden_trabajos.id', '=', 'historia_ordenes.idorden')
                ->select('orden_trabajos.id','num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','orden_trabajos.fecha_entrega','orden_trabajos.fechai','personas.id_cod_persona','personas.tipo','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','historia_ordenes.idinpes','orden_trabajos.estado_pago')
                ->where('personas.tipo', '=', '1')
                ->orderBy('orden_trabajos.id','desc')->paginate(10);
            }
            else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
                $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
                ->join('users','idusuario','=','users.id')
                ->join('pago_ordenes','orden_trabajos.id','=','pago_ordenes.idordentrabajo')
                ->join('historia_ordenes', 'orden_trabajos.id', '=', 'historia_ordenes.idorden')
                ->select('orden_trabajos.id','num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','orden_trabajos.fecha_entrega','orden_trabajos.fechai','personas.id_cod_persona','personas.tipo','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','historia_ordenes.idinpes','orden_trabajos.estado_pago')
                ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
                ->where('personas.tipo', '=', '1')
                ->orderBy('orden_trabajos.id','desc')->paginate(10);
            }  
        }else{

            $buscar = $request->buscar;
            $criterio = $request->criterio;
            if($buscar==''){ 
               $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
               ->join('users','idusuario','=','users.id')
               ->join('pago_ordenes','orden_trabajos.id','=','pago_ordenes.idordentrabajo')
               ->join('historia_ordenes', 'orden_trabajos.id', '=', 'historia_ordenes.idorden')
               ->select('orden_trabajos.id','num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','fecha_entrega','orden_trabajos.fechai','personas.id_cod_persona','personas.tipo','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
               'pago_ordenes.idinpecpago','historia_ordenes.idinpes','orden_trabajos.estado_pago')
               ->where('personas.tipo', '=', '1')
               ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
               ->orderBy('orden_trabajos.id','desc')->paginate(10);
               // personas.id_cod_persona
               
            }
            
            else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
               $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
               ->join('users','idusuario','=','users.id')
               ->join('pago_ordenes','orden_trabajos.id','=','pago_ordenes.idordentrabajo')
               ->join('historia_ordenes', 'orden_trabajos.id', '=', 'historia_ordenes.idorden')
               ->select('orden_trabajos.id','num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','fecha_entrega','orden_trabajos.fechai','personas.id_cod_persona','personas.tipo','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
               'pago_ordenes.idinpecpago','historia_ordenes.idinpes','orden_trabajos.estado_pago')
               ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
               ->where('personas.tipo', '=', '1')
               ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
               ->orderBy('orden_trabajos.id','desc')->paginate(10);
             }
            

        }
 
         return [
             'pagination' => [
                 'total'         => $trabajos->total(),//el total de pag
                 'current_page'  => $trabajos->currentPage(), //pagina actual
                 'per_page'      => $trabajos->perPage(), //los registros de pagina
                 'last_page'     => $trabajos->lastPage(), // ultima pagina
                 'from'          => $trabajos->firstItem(),//desde la pagina
                 'to'            => $trabajos->lastItem()//hasta la pagina
             ],
             'trabajos' => $trabajos
            //  'autenticado' => $autenticado
         ];
    }

    public function indexEm(Request $request)
    {
        // $autenticado =  Auth::user()->id_rol;
        //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $buscar = $request->buscar;
         $criterio = $request->criterio;

        if(\Auth::user()->id_rol > 1){
 
            if($buscar==''){ 
                $trabajos = Ordentrabajo::join('personas','orden_trabajos.idpersona','=','personas.id')
                ->join('users','orden_trabajos.idusuario','=','users.id')
                ->join('pago_ordenes','orden_trabajos.id','=','pago_ordenes.pago_numor')
                ->join('historia_ordenes', 'orden_trabajos.id', '=', 'historia_ordenes.idorden')
                ->select('orden_trabajos.id','num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','orden_trabajos.fechai','fecha_entrega','personas.id_cod_persona','personas.tipo','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','historia_ordenes.idinpes')
                ->where('personas.tipo', '=', '2')
                ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
                ->orderBy('orden_trabajos.id','desc')->paginate(10);
            }

            
            else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
                $trabajos = Ordentrabajo::join('personas','orden_trabajos.idpersona','=','personas.id')
                ->join('users','orden_trabajos.idusuario','=','users.id')
                ->join('pago_ordenes','orden_trabajos.id','=','pago_ordenes.pago_numor')
                ->join('historia_ordenes', 'orden_trabajos.id', '=', 'historia_ordenes.idorden')
                ->select('orden_trabajos.id','num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','orden_trabajos.fechai','fecha_entrega','personas.id_cod_persona','personas.tipo','personas.nombre','orden_trabajos.total_orden','pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','historia_ordenes.idinpes')
                ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
                ->where('personas.tipo', '=', '2')
                ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
                ->orderBy('orden_trabajos.id','desc')->paginate(10);
                // ->where('orden_trabajos.estadoinven', '=', '1')
            }

        } else {

            if($buscar==''){ 
                $trabajos = Ordentrabajo::join('personas','orden_trabajos.idpersona','=','personas.id')
                ->join('users','orden_trabajos.idusuario','=','users.id')
                ->join('pago_ordenes','orden_trabajos.id','=','pago_ordenes.pago_numor')
                ->join('historia_ordenes', 'orden_trabajos.id', '=', 'historia_ordenes.idorden')
                ->select('orden_trabajos.id','orden_trabajos.num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','orden_trabajos.fechai','fecha_entrega',
                'personas.id_cod_persona','personas.tipo','personas.nombre','total_orden','pago_ordenes.monto',
                'pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','historia_ordenes.idinpes')
                ->where('personas.tipo', '=', '2')
                ->orderBy('orden_trabajos.id','desc')->paginate(10);
            }

            
            else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
                $trabajos = Ordentrabajo::join('personas','orden_trabajos.idpersona','=','personas.id')
                ->join('users','orden_trabajos.idusuario','=','users.id')
                ->join('pago_ordenes','orden_trabajos.id','=','pago_ordenes.pago_numor')
                ->join('historia_ordenes', 'orden_trabajos.id', '=', 'historia_ordenes.idorden')
                ->select('orden_trabajos.id','orden_trabajos.num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','orden_trabajos.fechai',
                'fecha_entrega','personas.id_cod_persona','personas.tipo','personas.nombre','orden_trabajos.total_orden',
                'pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','historia_ordenes.idinpes')
                ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
                ->where('personas.tipo', '=', '2')
                ->orderBy('orden_trabajos.id','desc')->paginate(10);
                // ->where('orden_trabajos.estadoinven', '=', '1')
            }

        }        
 
 
         return [
             'pagination' => [
                 'total'         => $trabajos->total(),//el total de pag
                 'current_page'  => $trabajos->currentPage(), //pagina actual
                 'per_page'      => $trabajos->perPage(), //los registros de pagina
                 'last_page'     => $trabajos->lastPage(), // ultima pagina
                 'from'          => $trabajos->firstItem(),//desde la pagina
                 'to'            => $trabajos->lastItem()//hasta la pagina
             ],
             'trabajos' => $trabajos
            //  'autenticado' => $autenticado
         ];
    }

    // Listar los trabajos de empresas para despacharlos 
    public function ordenDespa(Request $request)
    {
        // $autenticado =  Auth::user()->id_rol;
        //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
        if(\Auth::user()->id_rol > 1){
         $buscar = $request->buscar;
         $criterio = $request->criterio;

         if($buscar==''){ 
            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
            ->join('historia_ordenes', 'num_orden', '=', 'historia_ordenes.idorden')
            ->select('num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','orden_trabajos.fecha_entrega','orden_trabajos.fechai','personas.id_cod_persona','personas.tipo','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
            'pago_ordenes.idinpecpago','historia_ordenes.idinpes','historia_ordenes.id as historia','orden_trabajos.hora_entrega')
            ->where('personas.tipo', '=', '2')
            ->where('historia_ordenes.idinpes', '=', '5')
            ->where('orden_trabajos.estadoinven', '=', '1')
            ->where('orden_trabajos.estadoinven', '=', '1')
            ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
            ->orderBy('orden_trabajos.id','asc')->paginate(5);
            // personas.id_cod_persona
         }

         else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
            ->join('historia_ordenes', 'num_orden', '=', 'historia_ordenes.idorden')
            ->select('num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','orden_trabajos.fecha_entrega','orden_trabajos.fechai','personas.id_cod_persona','personas.tipo','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
            'pago_ordenes.idinpecpago','historia_ordenes.idinpes','orden_trabajos.hora_entrega')
            ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
            ->where('personas.tipo', '=', '2')
            ->where('historia_ordenes.idinpes', '=', '5')
            ->where('orden_trabajos.estadoinven', '=', '1')
            ->where('orden_trabajos.estadoinven', '=', '1')
            ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
            ->orderBy('orden_trabajos.id','asc')->paginate(5);
            
          }

        }else {
            $buscar = $request->buscar;
            $criterio = $request->criterio;
            if($buscar==''){ 
                $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
                ->join('users','idusuario','=','users.id')
                ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
                ->join('historia_ordenes', 'num_orden', '=', 'historia_ordenes.idorden')
                ->select('num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','orden_trabajos.fecha_entrega','orden_trabajos.fechai','personas.id_cod_persona','personas.tipo','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','historia_ordenes.idinpes','historia_ordenes.id as historia','orden_trabajos.hora_entrega')
                ->where('personas.tipo', '=', '2')
                ->where('historia_ordenes.idinpes', '=', '5')
                ->where('orden_trabajos.estadoinven', '=', '1')
                ->where('orden_trabajos.estadoinven', '=', '1')
                ->orderBy('orden_trabajos.id','asc')->paginate(5);
                // personas.id_cod_persona
             }
             
             else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
                $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
                ->join('users','idusuario','=','users.id')
                ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
                ->join('historia_ordenes', 'num_orden', '=', 'historia_ordenes.idorden')
                ->select('num_orden','orden_trabajos.estado','orden_trabajos.fecha_hora','orden_trabajos.fecha_entrega','orden_trabajos.fechai','personas.id_cod_persona','personas.tipo','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.pago_fecha_hora','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','historia_ordenes.idinpes','orden_trabajos.hora_entrega')
                ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
                ->where('personas.tipo', '=', '2')
                ->where('historia_ordenes.idinpes', '=', '5')
                ->where('orden_trabajos.estadoinven', '=', '1')
                ->where('orden_trabajos.estadoinven', '=', '1')
                ->orderBy('orden_trabajos.id','asc')->paginate(5);
              }

        }
 
 
         return [
             'pagination' => [
                 'total'         => $trabajos->total(),//el total de pag
                 'current_page'  => $trabajos->currentPage(), //pagina actual
                 'per_page'      => $trabajos->perPage(), //los registros de pagina
                 'last_page'     => $trabajos->lastPage(), // ultima pagina
                 'from'          => $trabajos->firstItem(),//desde la pagina
                 'to'            => $trabajos->lastItem()//hasta la pagina
             ],
             'trabajos' => $trabajos
             
            //  'autenticado' => $autenticado
         ];
    }
    // Lista de los trabajos clientes para despacharlos
    public function ordenDespaCli(Request $request)
    {
        // $autenticado =  Auth::user()->id_rol;
        //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
        if(\Auth::user()->id_rol > 1){
         $buscar = $request->buscar;
         $criterio = $request->criterio;
 
         if($buscar==''){ 
            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->join('historia_ordenes', 'num_orden', '=', 'historia_ordenes.idorden')
            ->select('num_orden','orden_trabajos.fecha_hora','orden_trabajos.fechai','orden_trabajos.horai','orden_trabajos.fecha_entrega','orden_trabajos.hora_entrega','personas.id_cod_persona','personas.tipo','personas.nombre','historia_ordenes.id as historia')
            ->where('personas.tipo', '=', '1')
            ->where('historia_ordenes.idinpes', '=', '5')
            ->where('orden_trabajos.estadoinven', '=', '1')
            ->where('orden_trabajos.estado_pago', '=', '1')
            ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
            ->orderBy('orden_trabajos.id','asc')->paginate(5);
            // personas.id_cod_persona
               
         }else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
            ->join('historia_ordenes', 'num_orden', '=', 'historia_ordenes.idorden')
            ->select('num_orden','orden_trabajos.fecha_hora','orden_trabajos.fechai','orden_trabajos.horai','orden_trabajos.fecha_entrega','orden_trabajos.hora_entrega','personas.id_cod_persona','personas.tipo','personas.nombre')
            ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
            ->where('personas.tipo', '=', '1')
            ->where('historia_ordenes.idinpes', '=', '5')
            ->where('orden_trabajos.estadoinven', '=', '1')
            ->where('orden_trabajos.estado_pago', '=', '1')
            ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
            ->orderBy('orden_trabajos.id','asc')->paginate(5);
            
          }
        }else{
            $buscar = $request->buscar;
            $criterio = $request->criterio;
 
         if($buscar==''){ 
            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->join('historia_ordenes', 'num_orden', '=', 'historia_ordenes.idorden')
            ->select('num_orden','orden_trabajos.fecha_hora','orden_trabajos.fechai','orden_trabajos.horai','orden_trabajos.fecha_entrega','orden_trabajos.hora_entrega','personas.id_cod_persona','personas.tipo','personas.nombre','historia_ordenes.id as historia')
            ->where('personas.tipo', '=', '1')
            ->where('historia_ordenes.idinpes', '=', '5')
            ->where('orden_trabajos.estadoinven', '=', '1')
            ->where('orden_trabajos.estado_pago', '=', '1')
            ->orderBy('orden_trabajos.id','asc')->paginate(5);

         }else {

            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
            ->join('historia_ordenes', 'num_orden', '=', 'historia_ordenes.idorden')
            ->select('num_orden','orden_trabajos.fecha_hora','orden_trabajos.fechai','orden_trabajos.horai','orden_trabajos.fecha_entrega','orden_trabajos.hora_entrega','personas.id_cod_persona','personas.tipo','personas.nombre')
            ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
            ->where('personas.tipo', '=', '1')
            ->where('historia_ordenes.idinpes', '=', '5')
            ->where('orden_trabajos.estadoinven', '=', '1')
            ->where('orden_trabajos.estado_pago', '=', '1')
            ->orderBy('orden_trabajos.id','asc')->paginate(5);

         }

        }
 
         return [
             'pagination' => [
                 'total'         => $trabajos->total(),//el total de pag
                 'current_page'  => $trabajos->currentPage(), //pagina actual
                 'per_page'      => $trabajos->perPage(), //los registros de pagina
                 'last_page'     => $trabajos->lastPage(), // ultima pagina
                 'from'          => $trabajos->firstItem(),//desde la pagina
                 'to'            => $trabajos->lastItem()//hasta la pagina
             ],
             'trabajos' => $trabajos
            //  'autenticado' => $autenticado
         ];
    }

    public function obtenerCabecera(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        
        $id = $request->id;
        $pago = $request->pago;
        
           $trabajo = Ordentrabajo::join('personas','idpersona','=','personas.id')
           ->join('users','idusuario','=','users.id')
           ->join('pago_ordenes', 'orden_trabajos.id', '=','pago_ordenes.idordentrabajo')
           ->join('historia_ordenes', 'orden_trabajos.id', '=', 'historia_ordenes.idorden')
           ->select('personas.nombre','orden_trabajos.id','orden_trabajos.fecha_hora','orden_trabajos.fecha_entrega','personas.id_cod_persona','personas.tipo','observacion','total_orden',
           'users.usuario','pago_ordenes.idformapago as idforma','pago_ordenes.idmediopago as idmedio','pago_ordenes.monto as monto','historia_ordenes.idinpes')
           ->where('orden_trabajos.id', '=', $id)
           ->orderBy('orden_trabajos.id','desc')->take(1)->get();

            return [
                'trabajo' => $trabajo
            ];

    }

    public function obtenerDetalles(Request $request)
    {
         //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $id = $request->id;
        //  ->join('users','idusuario','=','users.id')
            $prendas = Ordenprenda::join('prendas','orden_prendas.idprendas','=','prendas.id')
            ->select('orden_prendas.id','orden_prendas.cantiprendas','orden_prendas.precio','prendas.nombre as prenda', 'orden_prendas.idprendas')
            ->where('orden_prendas.idordentra', '=', $id)
            ->orderBy('orden_prendas.id','desc')->get();
 
             return [
                 'prendas' => $prendas,
             ];

    }

    public function obtenerDetallesEmpresa(Request $request)
    {
         //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $id = $request->id;
        //  ->join('users','idusuario','=','users.id')
            $prendas = Ordenprenda::join('prendaempresas','orden_prendas.idprendas','=','prendaempresas.id')
            ->select('orden_prendas.id','orden_prendas.cantiprendas','orden_prendas.precio','prendaempresas.nombre as prenda', 'orden_prendas.idprendas')
            ->where('orden_prendas.prenda_numor', '=', $id)
            ->orderBy('orden_prendas.id','desc')->get();
 
             return [
                 'prendas' => $prendas
             ];

    }


    public function obtenerDetallesTrabajo(Request $request)
    {
         //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $id = $request->id;
        //  ->join('users','idusuario','=','users.id')
            $tipotras = Ordentipotra::join('tipotrabajos', 'orden_tipotras.id_tipotrabajo','=','tipotrabajos.id')
            // join('prendas','orden_prendas.idprendas','=','prendas.id')
            // ->select('orden_prendas.cantiprendas','orden_prendas.precio','prendas.nombre as prenda')
            ->select('orden_tipotras.id_prenda','tipotrabajos.nombre_tra as traba')
            ->where('orden_tipotras.prenda_numor', '=', $id)
            ->orderBy('orden_tipotras.id','desc')->get();
 
             return [
                 'tipotras' => $tipotras
             ];

    }

    public function egresados(Request $request)
    {
        //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $buscar = $request->buscar;
         $criterio = $request->criterio;
 
         if($buscar==''){ 
            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('guia_despachos','num_orden','=','guia_despachos.guia_ordent')
            ->select('num_orden','orden_trabajos.fecha_hora','personas.id_cod_persona','personas.nombre')
            ->where('orden_trabajos.estado', '=', '0')
            ->orderBy('orden_trabajos.id','desc')->paginate(8);
            // personas.id_cod_persona
            
         }
         
         else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('guia_despachos','num_orden','=','guia_despachos.guia_ordent')
            ->select('num_orden','orden_trabajos.fecha_hora','personas.id_cod_persona','personas.nombre')
            ->where('orden_trabajos.estado', '=', '0')
            ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
            ->orderBy('orden_trabajos.id','desc')->paginate(8);
          }
 
 
         return [
             'pagination' => [
                 'total'         => $trabajos->total(),//el total de pag
                 'current_page'  => $trabajos->currentPage(), //pagina actual
                 'per_page'      => $trabajos->perPage(), //los registros de pagina
                 'last_page'     => $trabajos->lastPage(), // ultima pagina
                 'from'          => $trabajos->firstItem(),//desde la pagina
                 'to'            => $trabajos->lastItem()//hasta la pagina
             ],
             'trabajos' => $trabajos
         ];
    }

    public function obtenerCabeceraEgresado(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        
        $id = $request->id;
        $pago = $request->pago;
        
           $trabajo = Ordentrabajo::join('personas','idpersona','=','personas.id')
           ->join('users','idusuario','=','users.id')
           ->join('pago_ordenes', 'num_orden', '=','pago_ordenes.pago_numor')
           ->join('historia_ordenes', 'num_orden', '=', 'historia_ordenes.idorden')
           ->select('personas.nombre','num_orden','orden_trabajos.fecha_hora','orden_trabajos.fecha_entrega','personas.id_cod_persona','personas.tipo','observacion','total_orden',
           'users.usuario','pago_ordenes.idformapago as idforma','pago_ordenes.idmediopago as idmedio','pago_ordenes.monto as monto','historia_ordenes.idinpes')
           ->where('num_orden', '=', $id)
           ->where('pago_ordenes.idinpecpago', '=', '2')
           ->orderBy('num_orden','desc')->take(1)->get();

            return [
                'trabajo' => $trabajo
            ];

    }

    public function pdf(Request $request, $id)
    {
        //solo peticiones ajax
         

        $trabajo = Ordentrabajo::join('personas','idpersona','=','personas.id')
        ->join('users','idusuario','=','users.id')
        ->join('pago_ordenes', 'num_orden', '=','pago_ordenes.pago_numor')

        ->select('personas.nombre','num_orden','fecha_hora','fecha_entrega','personas.id_cod_persona','personas.direccion','personas.telefono','personas.email','observacion','total_orden',
        'users.usuario','users.nombre as nomusu','pago_ordenes.idinpecpago','pago_ordenes.monto as monto')
        ->where('num_orden', '=', $id)
        ->orderBy('num_orden','desc')->take(1)->get();

        //para traer la forma de pago
        $pago = Pagoorden::join('formapagos', 'idformapago','=', 'formapagos.id')
        ->select('nombre')
        ->where('pago_numor', '=', $id)->get();

        //para traer el medio de pago
         $medio = Pagoorden::join('mediopagos', 'idmediopago','=', 'mediopagos.id')
         ->select('nombre')
         ->where('pago_numor', '=', $id)->get();

         $estadopago =Pagoorden::join('inpecpagos', 'idinpecpago','=', 'inpecpagos.id')
         ->select('nombre')
         ->where('pago_numor', '=', $id)->get();
        

        $prendas = Ordenprenda::join('prendas','orden_prendas.idprendas','=','prendas.id')
        ->select('orden_prendas.cantiprendas','orden_prendas.precio','prendas.nombre as prenda', 'orden_prendas.idprendas')
        ->where('orden_prendas.prenda_numor', '=', $id)
        ->orderBy('orden_prendas.id','desc')->get();

        $numorden = OrdenTrabajo::select('num_orden')->where('id', $id)->get();

        $local = OrdenTrabajo::join('ubicaciones', 'orden_trabajos.idubicacion','=', 'ubicaciones.id')
        ->select('ubicaciones.local')
        ->where('orden_trabajos.num_orden', '=', $id)
        ->get();

        $pdf = \PDF::loadView('pdf.orden',['trabajo'=>$trabajo,'prenda'=>$prendas,'pago'=>$pago,'medio'=>$medio,'local'=>$local,'estadopago'=>$estadopago]);
        return $pdf->stream('Orden de trabajo N°'.$numorden[0]->num_orden.'.pdf');
    }


    public function pdfEmpresa(Request $request, $id)
    {
        //solo peticiones ajax
         

        $trabajo = Ordentrabajo::join('personas','idpersona','=','personas.id')
        ->join('users','idusuario','=','users.id')
        ->join('pago_ordenes', 'num_orden', '=','pago_ordenes.pago_numor')

        ->select('personas.nombre','num_orden','fecha_hora','fecha_entrega','personas.id_cod_persona','personas.direccion','personas.telefono','personas.email','observacion','total_orden',
        'users.usuario','users.nombre as nomusu','pago_ordenes.idinpecpago','pago_ordenes.monto as monto')
        ->where('num_orden', '=', $id)
        ->orderBy('num_orden','desc')->take(1)->get();

        //para traer la forma de pago
        $pago = Pagoorden::join('formapagos', 'idformapago','=', 'formapagos.id')
        ->select('nombre')
        ->where('pago_numor', '=', $id)->get();

        //para traer el medio de pago
         $medio = Pagoorden::join('mediopagos', 'idmediopago','=', 'mediopagos.id')
         ->select('nombre')
         ->where('pago_numor', '=', $id)->get();

         $estadopago =Pagoorden::join('inpecpagos', 'idinpecpago','=', 'inpecpagos.id')
         ->select('nombre')
         ->where('pago_numor', '=', $id)->get();
        

        $prendas = Ordenprenda::join('prendaempresas','orden_prendas.idprendas','=','prendaempresas.id')
        ->select('orden_prendas.cantiprendas','orden_prendas.precio','prendaempresas.nombre as prenda', 'orden_prendas.idprendas')
        ->where('orden_prendas.prenda_numor', '=', $id)
        ->orderBy('orden_prendas.id','desc')->get();

        $numorden = OrdenTrabajo::select('num_orden')->where('id', $id)->get();

        $local = OrdenTrabajo::join('ubicaciones', 'orden_trabajos.idubicacion','=', 'ubicaciones.id')
        ->select('ubicaciones.local')
        ->where('orden_trabajos.num_orden', '=', $id)
        ->get();

        $pdf = \PDF::loadView('pdf.orden',['trabajo'=>$trabajo,'prenda'=>$prendas,'pago'=>$pago,'medio'=>$medio,'local'=>$local,'estadopago'=>$estadopago]);
        return $pdf->stream('Orden de trabajo N°'.$numorden[0]->num_orden.'.pdf');
    }

    public function pdfnoprecio(Request $request, $id)
    {

        $trabajo = Ordentrabajo::join('personas','idpersona','=','personas.id')
        ->join('users','idusuario','=','users.id')
        ->join('pago_ordenes', 'num_orden', '=','pago_ordenes.pago_numor')

        ->select('personas.nombre','num_orden','fecha_hora','fecha_entrega','personas.id_cod_persona','personas.direccion','personas.telefono','personas.email','observacion','total_orden',
        'users.usuario','users.nombre as nomusu','pago_ordenes.idinpecpago','pago_ordenes.monto as monto')
        ->where('num_orden', '=', $id)
        ->orderBy('num_orden','desc')->take(1)->get();

        //para traer la forma de pago
        $pago = Pagoorden::join('formapagos', 'idformapago','=', 'formapagos.id')
        ->select('nombre')
        ->where('pago_numor', '=', $id)->get();

        //para traer el medio de pago
         $medio = Pagoorden::join('mediopagos', 'idmediopago','=', 'mediopagos.id')
         ->select('nombre')
         ->where('pago_numor', '=', $id)->get();

         $estadopago =Pagoorden::join('inpecpagos', 'idinpecpago','=', 'inpecpagos.id')
         ->select('nombre')
         ->where('pago_numor', '=', $id)->get();
        

        $prendas = Ordenprenda::join('prendaempresas','orden_prendas.idprendas','=','prendaempresas.id')
        ->select('orden_prendas.cantiprendas','orden_prendas.precio','prendaempresas.nombre as prenda', 'orden_prendas.idprendas')
        ->where('orden_prendas.prenda_numor', '=', $id)
        ->orderBy('orden_prendas.id','desc')->get();

        $numorden = OrdenTrabajo::select('num_orden')->where('id', $id)->get();

        $local = OrdenTrabajo::join('ubicaciones', 'orden_trabajos.idubicacion','=', 'ubicaciones.id')
        ->select('ubicaciones.local')
        ->where('orden_trabajos.num_orden', '=', $id)
        ->get();

        $pdf = \PDF::loadView('pdf.ordenoprecio',['trabajo'=>$trabajo,'prenda'=>$prendas,'pago'=>$pago,'medio'=>$medio,'local'=>$local,'estadopago'=>$estadopago]);
        return $pdf->stream('Orden de trabajo N°'.$numorden[0]->num_orden.'.pdf');
    }


    public function pdfegre(Request $request, $id, $id2)
    {
        
        $trabajo = Ordentrabajo::join('personas','idpersona','=','personas.id')
        ->join('users','idusuario','=','users.id')
        ->join('pago_ordenes', 'num_orden', '=','pago_ordenes.pago_numor')
        ->select('personas.nombre','num_orden','fecha_entrega','personas.id_cod_persona','personas.direccion','personas.telefono','personas.email','observacion','total_orden',
        'users.usuario','users.nombre as nomusu','pago_ordenes.idinpecpago')
        ->where('num_orden', '=', $id)
        ->orderBy('num_orden','desc')->take(1)->get();

        //para traer la forma de pago independiente
        $montoegre = Pagoorden::select('monto')->where('pago_ordenes.id', '=', $id2)->get();   
        $entregado = Pagoorden::select('pago_fecha_hora')->where('pago_ordenes.id', '=', $id2)->get();

        $pago = Pagoorden::join('formapagos', 'idformapago','=', 'formapagos.id')
        ->select('nombre')
        ->where('pago_ordenes.id', '=', $id2)->get();

        //para traer el medio de pago
         $medio = Pagoorden::join('mediopagos', 'idmediopago','=', 'mediopagos.id')
         ->select('nombre')
         ->where('pago_ordenes.id', '=', $id2)->get();
        
         $estadopago =Pagoorden::join('inpecpagos', 'idinpecpago','=', 'inpecpagos.id')
         ->select('nombre')
         ->where('pago_ordenes.id', '=', $id2)->get();

        $prendas = Ordenprenda::join('prendas','orden_prendas.idprendas','=','prendas.id')
        ->select('orden_prendas.cantiprendas','orden_prendas.precio','prendas.nombre as prenda', 'orden_prendas.idprendas')
        ->where('orden_prendas.prenda_numor', '=', $id)
        ->orderBy('orden_prendas.id','desc')->get();

        $numorden = OrdenTrabajo::select('num_orden')->where('id', $id)->get();

        $local = OrdenTrabajo::join('ubicaciones', 'orden_trabajos.idubicacion','=', 'ubicaciones.id')
        ->select('ubicaciones.local')
        ->where('orden_trabajos.num_orden', '=', $id)
        ->get();

        $pdf = \PDF::loadView('pdf.ordenegre',['trabajo'=>$trabajo,'prenda'=>$prendas,'pago'=>$pago,'medio'=>$medio,'local'=>$local,'estadopago'=>$estadopago,'montoegre'=>$montoegre,'entregado'=>$entregado]);
        return $pdf->stream('Orden de trabajo N°'.$numorden[0]->num_orden.'.pdf');
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
        try{
            DB::beginTransaction();
 
            $mytime= Carbon::now('America/Santiago');
 
            $trabajo = new Ordentrabajo();
            $trabajo->num_orden = $request->num_orden;
            $trabajo->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $trabajo->idpersona = $request->id_persona;
            $trabajo->nombre = $request->cliente;
            $trabajo->total_orden = $request->total_orden;
            $trabajo->observacion = $request->observacion;
            $trabajo->fecha_hora = $mytime->toDateTimeString();

            //Creado nuevamente
            $trabajo->fechai = $request->fechai;
            $trabajo->horai = $request->horai;

            // Mofificados ultima mente
            $trabajo->fecha_entrega = $request->fecha_entrega;
            $trabajo->hora_entrega = $request->hora_entrega;
            //
            $trabajo->idubicacion = \Auth::user()->direccion;

            if($request->idformapago == 1){
                
                $trabajo->estado_pago = 1; //pago completo o no
            }else {
                $trabajo->estado_pago = 0;
            }
            $trabajo->estado = 1; //Ingresado o despachado
            $trabajo->estadoinven = 1; //Prendas en stock
            $trabajo->idinpes = 1;
            $trabajo->save();

            $detalles = $request->data;//Array de detalles
            
            //Recorro todos los elementos
            foreach($detalles as $ep=>$det)
            {
                $detalle = new Ordenprenda();
                $detalle->idordentra = $trabajo->id;
                $detalle->prenda_numor = $trabajo->num_orden;
                $detalle->idprendas = $det['idprenda'];
                $detalle->cantiprendas = $det['cantidad'];
                $detalle->stockprendas = $det['cantidad'];
                // $detalle->idtipotrabajo = '1';
                $detalle->precio = $det['precio'];
                $detalle->idinpes = '1';

                // Modificado nuevamente
                $detalle->fecha = $request->fechai;

                $detalle->save();

                $arrayTra = $det['arrayTrabajoCombo'];

                foreach($arrayTra as $ep=>$detra){
                    $tra = new Ordentipotra();
                    $tra->id_tipotrabajo = $detra['idtra'];
                    $tra->id_prenda = $detra['prendatrabaid'];
                    $tra->id_ordenpren = $detalle->id;
                    $tra->estado = '1'; //estado de trabajo para manejar luego los trabajos aplicado a cada prenda individual
                    $tra->prenda_numor = $trabajo->id;
                    $tra->save();
                }  

                //  Lo que se le inserta al resumen para usarlo en Estado de Pago
                $resumen = new Resumentrabajo();
                $resumen->ordent = $trabajo->num_orden;
                $resumen->idempresa = $trabajo->idpersona;
                $resumen->empresa = $trabajo->nombre;
                $resumen->idprendas = $det['idprenda'];
                $resumen->precio = $det['precio'];
                $resumen->cantidad = $det['cantidad'];
                $resumen->nombre = $det['prenda'];

                //Moificado nuevamente se guarda la en la que ingreso porq esto
                $resumen->fecha = $trabajo->fechai = $request->fechai;
                //
                $resumen->tipo = '1';
                $resumen->save();
                //  Lo que se le inserta al resumen

                 
                
            }

            $pago = new Pagoorden();
            $pago_fecha_hora =  $request->fechai .' '.$request->horai;
            $pago->idformapago = $request->idformapago;
            $pago->idmediopago = $request->idmediopago;
            $pago->idordentrabajo = $trabajo->id;
            $pago->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $pago->idinpecpago = '1';
            $pago->pago_numor = $request->num_orden;
            $pago->pago_fecha_hora = $pago_fecha_hora;
            $pago->estado = '1'; ///esto sera para manejar luego los pago realizados para cada prenda ejemplocon los hoteles
            if($request->idformapago == 1){
                // $pago->monto = $request->monto;
                $pago->monto = $trabajo->total_orden;
            }

            if($request->idformapago == 2){
                // $pago->monto = $request->monto;
                $pago->monto = $request->monto;
            }

            if($request->idformapago == 3){
                $pago->monto = '0';   
            }
            $pago->save();

            $histo = new Historiaordene();
            $histo->idorden = $trabajo->id; //id de la trabla orden
            $histo->idinpes = 1;
            $histo->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $histo->fecha_hora = $request->fechai;
            $histo->entrega = $request->fecha_entrega;
            $histo->histo_numor = $request->num_orden; //numero de la orden
            $histo->tipo = $request->tipo; //numero de la orden

            $histo->save();
 
            DB::commit();
        

        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function egresar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();

            $mytime= Carbon::now('America/Santiago');
            $pago_fecha_hora =  $request->date .' '.$request->time;

            $pago = new Pagoorden();
            $pago->idformapago = $request->idformapago;
            $pago->idmediopago = $request->idmediopago;
            $pago->idordentrabajo = $request->num_orden;
            $pago->idinpecpago = '2';
            $pago->pago_numor = $request->num_orden;
            $pago->pago_fecha_hora = $pago_fecha_hora;
            $pago->estado = 0;
            $pago->idusuario = \Auth::user()->id; //me guarde el usuario autenticado

            

            if($request->idformapago == 1){
                // $pago->monto = $request->monto;
                 $pago->monto = 0.0;
            }

            if($request->idformapago == 2){
                // $pago->monto = $request->monto;
                $pago->monto = $request->restante;
            }

            if($request->idformapago == 3){
                $pago->monto = $request->restante;
            }
            

            $pago->save();
            
            $orden = Ordentrabajo::findOrFail($request->num_orden);
            $orden->estado_pago = 1; //los estado son parareferirse a los pagos del la oden de trabajo
            $orden->save();

            
            DB::commit();
            
        } catch (Exception $e){
            DB::rollBack();
        }
 
            
    }

    public function costos(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

            $orden = Ordentrabajo::findOrFail($request->id);
            $orden->total_orden = $request->total2;
            $orden->save();

            $detalles = $request->data;//Array de detalles
            //Recorro todos los elementos
            foreach($detalles as $ep=>$det)
            {
                $orden = Ordenprenda::findOrFail($det['id']);
                $orden->precio = $det['precio'];
                $orden->save();
            }

    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
         
            $user = User::findOrFail($request->id);
            // $persona = Persona::findOrFail($user->id);
            $user->usuario = $request->usuario;
            $user->nombre = $request->nombre;
            $user->direccion = $request->direccion;
            $user->telefono = $request->telefono;
            $user->email = $request->email;
            $user->password = bcrypt( $request->password);
            $user->id_rol = $request->idrol;
            $user->condicion = '1';
            $user->save();
 
 
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $trabajo = Ordentrabajo::findOrFail($request->id);
        $trabajo->estado = '0';
        $trabajo->save();
    }

    public function codigostra(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = OrdenTrabajo::all();
        
        return [
            'codigo' => $codigo->last()
        ];
        
    }

    public function caja(Request $request)
    {
        
        $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
            ->select('num_orden','fecha_hora','fecha_entrega','personas.id_cod_persona','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.idmediopago',
            'pago_ordenes.idinpecpago'
            )
            ->orderBy('num_orden','asc')->paginate(7);
    }

    public function editarFecha(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
         
            $orden = Ordentrabajo::findOrFail($request->id);
            // $persona = Persona::findOrFail($user->id);
            $orden->fecha_hora = $request->fecha;
            
            $orden->save();
 
 
    }
    
 
   
 
}
