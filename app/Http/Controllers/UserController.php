<?php

namespace App\Http\Controllers;
use App\User;
//  use App\Rol;
// use Illuminate\Support\Facades\DB; //para trabajar con transacciones
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
      //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $buscar = $request->buscar;
         $criterio = $request->criterio;
 
         if($buscar==''){ 
            // $users=User::orderBy('id','desc')->paginate(7);
            $users=User::join('roles','users.id_rol','=','roles.id')
            ->join('ubicaciones','users.direccion','=','ubicaciones.id')
            ->select('users.id','users.nombre','ubicaciones.local as local','users.telefono','users.email','users.usuario','roles.nombre as rol','users.id','users.condicion','users.direccion')
            ->orderBy('users.id','desc')->paginate(10);

         }
         
         else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
            $users=User::join('roles','users.id_rol','=','roles.id')
            ->join('ubicaciones','users.direccion','=','ubicaciones.id')
            ->select('users.id','users.nombre','ubicaciones.local as local','users.telefono','users.email','users.usuario','roles.nombre as rol','users.id','users.direccion')
            ->where('users.'.$criterio, 'like', '%'. $buscar . '%')
            ->orderBy('users.id','desc')->paginate(10);
          }
 
 
         return [
             'pagination' => [
                 'total'         => $users->total(),//el total de pag
                 'current_page'  => $users->currentPage(), //pagina actual
                 'per_page'      => $users->perPage(), //los registros de pagina
                 'last_page'     => $users->lastPage(), // ultima pagina
                 'from'          => $users->firstItem(),//desde la pagina
                 'to'            => $users->lastItem()//hasta la pagina
             ],
             'users' => $users
         ];
    }

    public function store(Request $request)
    {   
        // if (!$request->ajax()) return redirect('/'); 
        $user = new User();//instacia de la clase persona
        $user->usuario = $request->usuario;
        $user->nombre = $request->nombre;
        $user->direccion = $request->direccion;
        $user->telefono = $request->telefono;
        $user->email = $request->email;
        $user->password = bcrypt( $request->password);
        $user->condicion = '1';
        $user->id_rol = $request->idrol;

        $user->save();
    }

    public function update(Request $request)
    {
        // if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->usuario = $request->usuario;
        $user->nombre = $request->nombre;
        $user->direccion = $request->direccion;
        $user->telefono = $request->telefono;
        $user->email = $request->email;
        $user->password = bcrypt( $request->password);
        $user->id_rol = $request->idrol;
        $user->condicion = '1';
        $user->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->condicion = '0';
        $user->save();
    }
 
    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->condicion = '1';
        $user->save();
    }
 
}
