<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; //para la hora actual
use App\Ordentrabajo;
use App\Ordenprenda;
use App\Pagoorden;
use App\Ordentipotra;
use App\Formapago;
use App\Mediopago;
use App\Historiaordene;


class CajaController extends Controller
{
    public function index(Request $request)
    {
        //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         if(\Auth::user()->id_rol > 1){
         $buscar = $request->buscar;
         $criterio = $request->criterio;
 
         if($buscar==''){ 

            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
            ->select('num_orden','fechai','fecha_entrega','personas.id_cod_persona','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.idmediopago',
            'pago_ordenes.idinpecpago','pago_ordenes.idformapago','pago_ordenes.pago_fecha_hora')
            ->where('pago_ordenes.idinpecpago', '=' , '1')
            ->where('personas.tipo', '=', '1')
            ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
            ->get();
            // ->orderBy('num_orden','asc')->paginate(7);
            // personas.id_cod_persona
            
         }
         
         else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('users','idusuario','=','users.id')
            ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
            ->select('num_orden','fechai','fecha_entrega','personas.id_cod_persona','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.idmediopago',
            'pago_ordenes.idinpecpago','pago_ordenes.idformapago','pago_ordenes.pago_fecha_hora')
            ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
            ->where('pago_ordenes.idinpecpago', '=' , '1')
            ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
            ->where('personas.tipo', '=', '1')
            ->get();
            // ->orderBy('num_orden','desc')->paginate(6);
            // ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
          }
 
        }else{
            $buscar = $request->buscar;
            $criterio = $request->criterio;

            if($buscar==''){

                $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
                ->join('users','idusuario','=','users.id')
                ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
                ->select('num_orden','fechai','fecha_entrega','personas.id_cod_persona','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','pago_ordenes.idformapago','pago_ordenes.pago_fecha_hora')
                ->where('pago_ordenes.idinpecpago', '=' , '1')
                ->where('personas.tipo', '=', '1')
                ->get();
                
            }else{

                $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
                ->join('users','idusuario','=','users.id')
                ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
                ->select('num_orden','fechai','fecha_entrega','personas.id_cod_persona','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','pago_ordenes.idformapago','pago_ordenes.pago_fecha_hora')
                ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
                ->where('pago_ordenes.idinpecpago', '=' , '1')
                ->where('personas.tipo', '=', '1')
                ->get();
                // ->orderBy('num_orden','desc')->paginate(6);
                // ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
            }
        }
         return [
             'trabajos' => $trabajos
         ];
    }


    public function despachado(Request $request)
    {
          //solo peticiones ajax
          if(!$request->ajax()) return redirect('/');
          $buscar = $request->buscar;
          $criterio = $request->criterio;

          if(\Auth::user()->id_rol > 1){
            
    
            if($buscar==''){ 
                $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
                ->join('users','idusuario','=','users.id')
                ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
                ->select('num_orden','fechai','fecha_entrega','personas.id_cod_persona','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','pago_ordenes.idformapago','pago_ordenes.pago_fecha_hora')
                ->where('pago_ordenes.idinpecpago', '=' , '2')
                ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
                ->get();
                // personas.id_cod_persona
                
            }
            
            else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
                $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
                ->join('users','idusuario','=','users.id')
                ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
                ->select('num_orden','fechai','fecha_entrega','personas.id_cod_persona','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.idmediopago',
                'pago_ordenes.idinpecpago','pago_ordenes.idformapago','pago_ordenes.pago_fecha_hora')
                ->where('pago_ordenes.idinpecpago', '=' , '2')
                ->where('orden_trabajos.idusuario', '=', \Auth::user()->id)
                ->where('pago_ordenes.pago_fecha_hora', 'like', '%'. $buscar . '%')
                //  ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
                
                ->get();
                // ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
            }
            }else{
                if($buscar==''){ 
                    $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
                    ->join('users','idusuario','=','users.id')
                    ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
                    ->select('num_orden','fechai','fecha_entrega','personas.id_cod_persona','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.idmediopago',
                    'pago_ordenes.idinpecpago','pago_ordenes.idformapago','pago_ordenes.pago_fecha_hora')
                    ->where('pago_ordenes.idinpecpago', '=' , '2')
                    ->get();
                    // personas.id_cod_persona
                    
                }
                
                else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
                    $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
                    ->join('users','idusuario','=','users.id')
                    ->join('pago_ordenes','num_orden','=','pago_ordenes.pago_numor')
                    ->select('num_orden','fechai','fecha_entrega','personas.id_cod_persona','personas.nombre','total_orden','pago_ordenes.monto','pago_ordenes.idmediopago',
                    'pago_ordenes.idinpecpago','pago_ordenes.idformapago','pago_ordenes.pago_fecha_hora')
                    ->where('pago_ordenes.idinpecpago', '=' , '2')
                    ->where('pago_ordenes.pago_fecha_hora', 'like', '%'. $buscar . '%')
                    //  ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
                    ->get();
                    // ->where('orden_trabajos.'.$criterio, 'like', '%'. $buscar . '%')
                }
            }
  
          return [
              'trabajos' => $trabajos
          ];
    
    }


    public function todosPago(Request $request)
    {
          //solo peticiones ajax
          if(!$request->ajax()) return redirect('/');
        
            $buscar = $request->buscar;
            $pagos = Pagoorden::select('idmediopago','idinpecpago','pago_fecha_hora','monto')
            ->where('pago_fecha_hora', 'like', '%'. $buscar . '%')
            ->where('idusuario', '=', \Auth::user()->id)
            ->get();
  
          return [
              'pagos' => $pagos
          ];
    
    }



   
}
