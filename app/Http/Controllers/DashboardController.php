<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; //para la hora actual
use Illuminate\Http\Request;
use App\Ordentrabajo;
use App\Historiaordene;
use App\Pagoorden;

class DashboardController extends Controller
{
    public function index(Request $request)
    {   
        $anio=date('Y');
        
        $ingresos=DB::table('orden_trabajos as t')
        ->select(DB::raw('MONTH(t.fecha_hora) as mes'),
                DB::raw('YEAR(t.fecha_hora) as anio'),
                DB::raw('SUM(t.total_orden) as total'))
        ->whereYear('t.fecha_hora', $anio)

        ->groupBy(DB::raw('MONTH(t.fecha_hora)'), DB::raw('YEAR(t.fecha_hora)'))
        ->get();

        return ['ingresos' => $ingresos, 'anio'=>$anio];


    }

    public function anterior(Request $request)
    {   
        
        $anio2=date('Y')-1;
        

        $ingresos=DB::table('orden_trabajos as t')
        ->select(DB::raw('MONTH(t.fecha_hora) as mes'),
                DB::raw('YEAR(t.fecha_hora) as anio'),
                DB::raw('SUM(t.total_orden) as total'))
        ->whereYear('t.fecha_hora', $anio2)

        ->groupBy(DB::raw('MONTH(t.fecha_hora)'), DB::raw('YEAR(t.fecha_hora)'))
        ->get();

        return ['ingresos' => $ingresos, 'anio2' =>$anio2];


    }




    public function pendientes(Request $request){
        
        if(\Auth::user()->id_rol > 1){
            $pendientes = DB::table('historia_ordenes as t') ->select(DB::raw('t.idinpes','t.idusuario'))
            ->where('t.idinpes','<=','4')
            ->where('t.idusuario', '=', \Auth::user()->id)->get();

            $listas = DB::table('historia_ordenes as t') ->select(DB::raw('t.idinpes','t.idusuario'))
            ->where('t.idinpes','=','5')
            ->where('t.idusuario', '=', \Auth::user()->id)->get();
        
        }else{
            $pendientes = DB::table('historia_ordenes as t') ->select(DB::raw('t.idinpes'))
            ->where('t.idinpes','<=','4')->get();

            $listas = DB::table('historia_ordenes as t') ->select(DB::raw('t.idinpes'))
            ->where('t.idinpes','=','5')->get();

        }
        return ['pendientes' => $pendientes, 'listas' => $listas];
    }


    public function ingdes(Request $request,$fecha){   
        $mytime= Carbon::now('America/Santiago');
        $hoy = $mytime->toDateTimeString();
        // $anio=date('D-M-Y');

        if(\Auth::user()->id_rol > 1){

            $ing=Ordentrabajo::select('num_orden','idusuario')
            ->where('estado', '=', '1')
            ->where('fecha_hora', 'like', '%'.$fecha.'%')
            ->where('idusuario', '=', \Auth::user()->id)->get();
            
            $desp=Ordentrabajo::select('num_orden','idusuario')
            ->where('estado', '=', '0')
            ->where('fecha_hora', 'like', '%'.$fecha.'%')
            ->where('idusuario', '=', \Auth::user()->id)->get();

        }else{
            $ing=Ordentrabajo::select('num_orden','idusuario')
            ->where('estado', '=', '1')
            ->where('fecha_hora', 'like', '%'.$fecha.'%')->get();
            
            $desp=Ordentrabajo::select('num_orden','idusuario')
            ->where('estado', '=', '0')
            ->where('fecha_hora', 'like', '%'.$fecha.'%')->get();
        }
        
        return ['ing' => $ing, 'desp' => $desp];
    }
    


}
