<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Prenda; //importando el modelo
use App\Prendaempresa; //importando el modelo
use App\Pren_precio; //importando el modelo
use App\Persona;

class PrendaController extends Controller
{
    
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        //esto es para realizar el buscar y viene por el request
        $buscar = $request->buscar;
        $criterio = $request->criterio; //aqui va el nombre del campo de la tabla a buscar

        if($buscar==''){
            $prenda=Prenda::orderBy('id','desc')->paginate(10); //pagination con elequent
        }else{
            //donde el texto buscar puede estar en el inicio o final de nuestro campo criterio
            $prenda=Prenda::where($criterio, 'like' , '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(8);
        }

        return [
            'paginacion' => [
                'total' => $prenda->total(),
                'pagina_actual' =>$prenda->currentPage(), //pag actual
                'pag_mostrar' =>$prenda->perPage(), ////numero de registros por pagina
                'pag_alfinal' => $prenda->lastPage(), //a cuantas paginas del final
                'pag_actual' =>$prenda->firstItem(), //pagina actualen la q se encuentra
                'pag_ultima' =>$prenda->lastItem(), //ultma Pagina
            ],
            'prendas' => $prenda
        ];
    }

    public function listarPrenda(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if($buscar==''){
            $prenda=Prenda::orderBy('nombre','asc')->paginate(10); //pagination con elequent
        }else{
            
            $prenda=Prenda::where($criterio, 'like' , '%'. $buscar . '%')->orderBy('nombre', 'asc')->paginate(8);
        }

        return [ 
                'pagination' => [
                    'total'         => $prenda->total(),//el total de pag
                    'current_page'  => $prenda->currentPage(), //pagina actual
                    'per_page'      => $prenda->perPage(), //los registros de pagina
                    'last_page'     => $prenda->lastPage(), // ultima pagina
                    'from'          => $prenda->firstItem(),//desde la pagina
                    'to'            => $prenda->lastItem()//hasta la pagina
                ],
                'prendas' => $prenda ];
    }

    public function listarPdf(){
        $prendas=Prenda::orderBy('id','desc')->get();
        $cont=Prenda::count();
        $pdf = \PDF::loadView('pdf.prendaspdf', [ 'prendas'=>$prendas, 'cont'=>$cont]);
        return $pdf->download('prendas.pdf');
    }

    public function buscarPrenda(Request $request){
        if(!$request->ajax()) return redirect('/');

        $filtro = $request->filtro;
        $prendas = Prenda::where('id','=', $filtro)
        ->select('id', 'nombre')->orderBy('nombre','asc')->take(1)->get(); /*que me tome un solo valor take*/

        return ['prendas' => $prendas];
    }
   
    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $prenda = new Prenda();
        $prenda->nombre = $request->nombre;
        $prenda->descripcion = $request->descripcion;
        $prenda->condicion = '1';
        $prenda->save();
    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $prenda = Prenda::findOrFail($request->id);//ojo
        $prenda->nombre = $request->nombre;
        $prenda->descripcion = $request->descripcion;
        $prenda->condicion = '1';
        $prenda->save();
    }

    public function desactivar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $prenda = Prenda::findOrFail($request->id);
        $prenda->condicion = '0';
        $prenda->save();

    }

    public function activar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $prenda = Prenda::findOrFail($request->id);
        $prenda->condicion = '1';
        $prenda->save();
    }


    // Controladores para Prenda Empresa

    public function indexPren(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        //esto es para realizar el buscar y viene por el request
        $buscar = $request->buscar;
        $criterio = $request->criterio; //aqui va el nombre del campo de la tabla a buscar

        if($buscar==''){
            $prenda=Prendaempresa::orderBy('id','desc')->paginate(10); //pagination con elequent
        }else{
            //donde el texto buscar puede estar en el inicio o final de nuestro campo criterio
            $prenda=Prendaempresa::where($criterio, 'like' , '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }

        return [
            'paginacion' => [
                'total' => $prenda->total(),
                'pagina_actual' =>$prenda->currentPage(), //pag actual
                'pag_mostrar' =>$prenda->perPage(), ////numero de registros por pagina
                'pag_alfinal' => $prenda->lastPage(), //a cuantas paginas del final
                'pag_actual' =>$prenda->firstItem(), //pagina actualen la q se encuentra
                'pag_ultima' =>$prenda->lastItem(), //ultma Pagina
            ],
            'prendas' => $prenda
        ];
    }
    public function storePren(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $prenda = new Prendaempresa();
        $prenda->nombre = $request->nombre;
        $prenda->descripcion = $request->descripcion;
        $prenda->save();
    }

    public function updatePren(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $prenda = Prendaempresa::findOrFail($request->id);//ojo
        $prenda->nombre = $request->nombre;
        $prenda->descripcion = $request->descripcion;
        $prenda->save();
    }

    public function desactivarPren(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $prenda = Prendaempresa::findOrFail($request->id);
        $prenda->condicion = '0';
        $prenda->save();

    }

    public function activarPren(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $prenda = Prendaempresa::findOrFail($request->id);
        $prenda->condicion = '1';
        $prenda->save();
    }

    public function buscarPrendaEm(Request $request){
        if(!$request->ajax()) return redirect('/');

        $filtro = $request->filtro;
        $cli = $request->cli;
        $prendas = Pren_precio::join('prendaempresas','idprendas','=','prendaempresas.id')
        ->where('prendaempresas.id','=', $filtro)
        ->where('pren_precios.idempresa','=', $cli)
        ->where('pren_precios.condicion', '=', '1')
        ->select('prendaempresas.id','prendaempresas.nombre','pren_precios.precio')->orderBy('prendaempresas.nombre','asc')->take(1)->get(); /*que me tome un solo valor take*/

        return ['prendas' => $prendas];
    }

    public function listarPrendaEm(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if($buscar==''){
            $prendas=Prendaempresa::orderBy('nombre','asc') //pagination con elequent
            ->where('prendaempresas.condicion', '=', '1')->paginate(10);
        }else{
            
            $prendas=Prendaempresa::where($criterio, 'like' , '%'. $buscar . '%')
            ->where('prendaempresas.condicion', '=', '1')
            ->orderBy('nombre', 'asc')->paginate(10);
        }

        return [ 
                'pagination' => [
                    'total'         => $prendas->total(),//el total de pag
                    'current_page'  => $prendas->currentPage(), //pagina actual
                    'per_page'      => $prendas->perPage(), //los registros de pagina
                    'last_page'     => $prendas->lastPage(), // ultima pagina
                    'from'          => $prendas->firstItem(),//desde la pagina
                    'to'            => $prendas->lastItem()//hasta la pagina
                ],
                'prendas' => $prendas ];
    }

    public function listarPrendaPrecio(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $cli = $request->cli;

        if($buscar==''){
            $prendas=Pren_precio::join('prendaempresas','idprendas','=','prendaempresas.id')
            ->where('pren_precios.idempresa', '=', $cli)
            ->where('pren_precios.condicion', '=', '1') 
            ->select('prendaempresas.id','prendaempresas.nombre','pren_precios.precio')->paginate(10); //pagination con elequent
        }else{
            
            $prendas=Pren_precio::join('prendaempresas','idprendas','=','prendaempresas.id')
            ->where('pren_precios.idempresa', '=', $cli)
            ->where('pren_precios.condicion', '=', '1')
            ->where('prendaempresas.'.$criterio, 'like' , '%'. $buscar . '%')->orderBy('nombre', 'asc')->paginate(10);
        }

        return [ 
                'pagination' => [
                    'total'         => $prendas->total(),//el total de pag
                    'current_page'  => $prendas->currentPage(), //pagina actual
                    'per_page'      => $prendas->perPage(), //los registros de pagina
                    'last_page'     => $prendas->lastPage(), // ultima pagina
                    'from'          => $prendas->firstItem(),//desde la pagina
                    'to'            => $prendas->lastItem()//hasta la pagina
                ],
                'prendas' => $prendas ];
    }

    public function listarPdfEm(){
        $prendas=Prendaempresa::orderBy('id','desc')->get();
        $cont=Prendaempresa::count();
        $pdf = \PDF::loadView('pdf.prendaspdf', [ 'prendas'=>$prendas, 'cont'=>$cont]);
        return $pdf->stream('prendas.pdf');
    }

    public function storePrecio(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();

            $detalles = $request->data;//Array de detalles
            $empresa = $request->idcliente;//Array de detalles

            foreach($detalles as $ep=>$det)
            {
                $detalle = new Pren_precio();
                $detalle->idempresa = $empresa;
                $detalle->idprendas = $det['idprenda'];
                $detalle->precio = $det['precio'];
                $detalle->save();
                
            }

            $person = Persona::findOrFail($request->idcliente);
            $person->prendas = 1;
            $person->save();

            DB::commit();
            
        } catch (Exception $e){
            DB::rollBack();
        }

    }

    public function verPrenda(Request $request)
    {
         //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $id = $request->id;
        //  ->join('users','idusuario','=','users.id')
            $prendas = Pren_precio::join('prendaempresas','pren_precios.idprendas','=','prendaempresas.id')
            ->select('pren_precios.id','pren_precios.precio','prendaempresas.nombre as prenda', 'pren_precios.idprendas')
            ->where('pren_precios.idempresa', '=', $id)
            ->where('pren_precios.condicion', '=', 1)
            ->orderBy('pren_precios.id','desc')->get();
 
             return [
                 'prendas' => $prendas
             ];

    }

    public function eliminarPrecio(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $prenda = Pren_precio::findOrFail($request->id);//ojo
        $prenda->idprendas = $request->prenda;
        $prenda->idempresa = $request->idcliente;
        $prenda->condicion = $request->condicion;
        $prenda->save();
    }

    public function cambiarPrecio(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $prenda = Pren_precio::findOrFail($request->id);//ojo
        $prenda->idprendas = $request->prenda;
        $prenda->idempresa = $request->idcliente;
        $prenda->precio = $request->precio;
        $prenda->save();
    }
                
            
}

