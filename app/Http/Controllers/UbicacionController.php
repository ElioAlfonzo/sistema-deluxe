<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ubicacion; //importando el modelo
class UbicacionController extends Controller
{
    public function index(Request $request)
    {
        // if(!$request->ajax()) return redirect('/');
        //esto es para realizar el buscar y viene por el request
        $buscar = $request->buscar;
        $criterio = $request->criterio; //aqui va el nombre del campo de la tabla a buscar

        if($buscar==''){
            $ubicacion=Ubicacion::orderBy('id','desc')->paginate(10); //pagination con elequent
        }else{
            //donde el texto buscar puede estar en el inicio o final de nuestro campo criterio
            $ubicacion=Ubicacion::where($criterio, 'like' , '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }

        return [
            'paginacion' => [
                'total' => $ubicacion->total(),
                'pagina_actual' =>$ubicacion->currentPage(), //pag actual
                'pag_mostrar' =>$ubicacion->perPage(), ////numero de registros por pagina
                'pag_alfinal' => $ubicacion->lastPage(), //a cuantas paginas del final
                'pag_actual' =>$ubicacion->firstItem(), //pagina actualen la q se encuentra
                'pag_ultima' =>$ubicacion->lastItem(), //ultma Pagina
            ],
            'ubicaciones' => $ubicacion
        ];
    }

   
   
    public function store(Request $request)
    {
        // if(!$request->ajax()) return redirect('/');
        $ubicacion = new Ubicacion();
        $ubicacion->direccion = $request->direccion;
        $ubicacion->local = $request->local;
        $ubicacion->estado = '1';
        $ubicacion->save();
    }

    

    
    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $ubicacion = Ubicacion::findOrFail($request->id);//ojo
        $ubicacion->direccion = $request->direccion;
        $ubicacion->local = $request->local;
        $ubicacion->estado= '1';
        $ubicacion->save();
    }

    public function desactivar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $ubicacion = Ubicacion::findOrFail($request->id);
        $ubicacion->estado = '0';
        $ubicacion->save();

    }

    public function activar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $ubicacion = Ubicacion::findOrFail($request->id);
        $ubicacion->estado = '1';
        $ubicacion->save();
    }

    public function selectUbicacion(Request $request)
    {
        $locales = Ubicacion::where('estado', '=', '1')
        ->select('id','local', 'direccion')
        ->orderBy('local','asc')->get();

        return ['locales' => $locales];
    }
}
