<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ordentrabajo;
use App\Ordenprenda;
use App\Pagoorden;
use App\Ordentipotra;
use App\Mediopago;
use App\Historiaordene;
use App\Inpestado;
use App\Persona;


class MovimientoController extends Controller
{
    public function index(Request $request)
    {
        //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $buscar = '';
        //  request->buscar;''
        //  $criterio = $request->criterio;
 
         if($buscar==''){ 
            $trabajos = Historiaordene::join('inpestados','idinpes','=','inpestados.id')
            ->join('orden_trabajos', 'histo_numor', '=', 'orden_trabajos.num_orden')
            ->select('histo_numor','inpestados.nombre','historia_ordenes.fecha_hora','orden_trabajos.fecha_entrega','historia_ordenes.idinpes','historia_ordenes.id','orden_trabajos.nombre')
            ->where('historia_ordenes.idinpes', '<' , '6')
            ->orderBy('historia_ordenes.id','desc')->get();
            // personas.id_cod_persona
            
            
         }
         
         else{//en caso de no estar vacio, buscar a inicio o al final del campo buscar
            $trabajos = Ordentrabajo::join('personas','idpersona','=','personas.id')
            ->join('inpestados','idinpes','=','inpestados.id')
            ->join('orden_trabajos', 'histo_numor', '=', 'orden_trabajos.num_orden')
            ->select('histo_numor','inpestados.nombre','historia_ordenes.fecha_hora')
            ->where('historia_ordenes.idinpes', '<' , '6')
            ->orderBy('historia_ordenes.id','desc')->get();
          }
          
          $personas = Persona::all();

         return [
             'trabajos' => $trabajos, 'personas' => $personas
         ];
    }

    


    public function update(Request $request)
    {
            $histo = Historiaordene::findOrFail($request->id);
            $histo->idinpes = $request->idinpes;
            $histo->save();
            
            // $detalle = Ordenprenda::findOrFail($request->id);
            // $detalle->idinpes = $request->idinpes;
            // $histo->save();
    }

    public function estados(Request $request)
    {
        
            $inpestado=Inpestado::orderBy('id','desc')->where('id', '<' , '6')->get(); //pagination con elequent

        return [
            'inpestados' => $inpestado
        ];
    }



}