<?php

namespace App\Exports;


use DB;
use App\Categoria;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CategoriasExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'Id',
            'Nombre',
            'Descripcion',
        ];
    }
    public function collection()
    {
         $categorias = DB::table('categorias')->select('id','nombre', 'descripcion')->get();
         return $categorias;
        
    }
}