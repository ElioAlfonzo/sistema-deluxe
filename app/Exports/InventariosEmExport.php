<?php
namespace App\Exports;
use DB;
use App\Historiaordene;
use App\Ubicacion;
use App\Prenda;
use App\Prendaempresa;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Http\Request;

class InventariosEmExport implements FromView
{
    private $cri;
    private $ini;
    private $fin;
    // Para poder recibir los parametros e imprir solo los de mi consulta
    public function __construct($cri,$ini,$fin)
    {

        $this->cri = $cri;
        $this->ini = $ini;
        $this->fin = $fin;
    }
    
    use Exportable;
    public function view(): View
    {

        if($this->cri== 0){
            $historias = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '2')
            ->where('historia_ordenes.idinpes', '<=', '5')
            ->where('orden_prendas.stockprendas', '>', '0')->get();
            
         }
        
         if($this->cri == 0 & $this->ini != '' & $this->fin != ''){
            $historias = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '2')
            ->where('historia_ordenes.idinpes', '<=', '5')
            ->where('orden_prendas.stockprendas', '>', '0')
            ->whereBetween('historia_ordenes.entrega',[$this->ini, $this->fin])->get();
        }

        if($this->cri != 0 & $this->ini == '' & $this->fin == '') {
            $historias = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')
            
           
            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '2')
            ->where('orden_prendas.stockprendas', '>', '0')
            ->where('historia_ordenes.idinpes', '=', $this->cri)->get();
         }
         
        if($this->cri !="0" & $this->ini != '' & $this->fin != '') {
            $historias = Historiaordene::join('orden_prendas','historia_ordenes.idorden','=','orden_prendas.idordentra')    
            ->join('inpestados','historia_ordenes.idinpes','=','inpestados.id')
            ->join('orden_trabajos','historia_ordenes.idorden','=','orden_trabajos.num_orden')
            ->join('personas','personas.id_cod_persona','=','orden_trabajos.idpersona')

            ->select('orden_prendas.idprendas','orden_prendas.cantiprendas','orden_prendas.stockprendas','inpestados.nombre as estados','historia_ordenes.idorden','orden_trabajos.fechai as recibo',
            'orden_trabajos.fecha_entrega as entrega','orden_trabajos.idubicacion','personas.nombre')
            ->where('historia_ordenes.tipo', '=', '2')
            ->where('historia_ordenes.idinpes', '=', $this->cri)
            ->where('orden_prendas.stockprendas', '>', '0')
            ->whereBetween('historia_ordenes.entrega',[$this->ini , $this->fin])->get();
        }
        
        $prendas = PrendaEmpresa::all();
        $ubicaciones = Ubicacion::all();
        
        

        return view('excel.inventariosEm', [
            'historias' =>  $historias,
            'ubicaciones' =>  $ubicaciones,
            'prendas' =>  $prendas
        ]);
    }

}