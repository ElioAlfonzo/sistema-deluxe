<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    public $table = "personas"; 
    
    protected $fillable = ['id_cod_persona','nombre','direccion','telefono','email','tipo','prendas'];

}

