<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado_pago extends Model
{
    public $table = "estado_pago";
    public $timestamps = false; 
    protected $fillable = [
        'folio',
        'fecha1',
        'fecha2',
        'fechaorigen',
        'status',
        'idempresa',
        'empresa',
        'neto',
        'iva',
        'total',
    ];
}
