<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formapago extends Model
{
    public $table = "formapagos";
    public $timestamps = false; 

    protected $fillable = [
        'nombre'
    ];

   
}
