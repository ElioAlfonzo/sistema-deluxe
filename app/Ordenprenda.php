<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordenprenda extends Model
{
    public $table = "orden_prendas";
    public $timestamps = false; 
    protected $fillable = [
        'idprendas',
        'idtipotrabajo',
        'idordentra',
        'precio',
        'prenda_numor',
        'cantiprendas',
        'stockprendas',
        'estado',
        'fecha'

    ];

    public function ordentrabajo(){ //muchas prendas pertenecen a una orden de trabajo
        return $this->belongsTo('App/Ordentrabajo');
    }
     
    public function tipotrabajos(){
        return $this->hasMany('App/Tipotrabajo');
    }

    public function prendas(){
        return $this->hasMany('App/Prenda');
    }
}
