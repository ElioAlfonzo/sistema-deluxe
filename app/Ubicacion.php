<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{   //aveces laravel no logra encontrar el pural de dicha palabra y mejor hacer esto
    public $table = "ubicaciones"; 
    
    protected $fillable = ['id','direccion','local','estado']; 
    public $timestamps = false;
}
