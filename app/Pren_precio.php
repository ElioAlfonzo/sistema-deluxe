<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pren_precio extends Model
{
    public $table = "pren_precios"; 
    protected $fillable = ['id','idprendas', 'idempresa','precio', 'condicion']; 
}