<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prenda extends Model
{
    public $table = "prendas"; 
    protected $fillable = ['id','nombre', 'descripcion', 'condicion']; 
}
