<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordentrabajo extends Model
{
    public $table = "orden_trabajos";

    protected $fillable = [
        'num_orden',
        'id_persona',
        'idusuario',
        'observacion',
        'fechai',
        'horai',
        'fecha_hora',
        'fecha_entrega',
        'hora_entrega',
        'total_orden',
        'estado',
        'estadoinven',
        'estadopago',


    ];

    public function usuario(){ //una orden de trabajo pertenece a un unico usuario
        return $this->belongsTo('App/User');
    }
     
    public function persona(){ //una orden de trabajo pertenece a un unico usuario
        return $this->belongsTo('App/Persona');
    }

    public function ordenprendas(){ //una orden de trabajo pertenece a un unico usuario
        return $this->hasMany('App/Ordenprenda');
    }
}
