<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historiaordene extends Model
{
    public $table = "historia_ordenes";
    // public $timestamps = false; 
    protected $fillable = [
        'idorden',
        'idinpes',
        'idubicacion',
        'fecha_hora',
        'histo_numor',
        'tipo'
    ];

}
