<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estadopago_prenda extends Model
{
    public $table = "estadopagos_prendas";
    public $timestamps = false; 
    protected $fillable = [
        'idprendas',
        'nombre',
        'idempresa',
        'empresa',
        'precio',
        'cantidad',
        'fecha',
    ];
}
