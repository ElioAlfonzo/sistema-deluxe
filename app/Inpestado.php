<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inpestado extends Model
{
    public $table = "inpestados"; 
    protected $fillable = ['id','nombre', 'descripcion', 'condicion']; 
}
