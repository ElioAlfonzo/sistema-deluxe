<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resumentrabajo extends Model
{
    public $table = "resumen_trabajos";
    public $timestamps = false; 
    protected $fillable = [
        'ordent',
        'guiades',
        'idempresa',
        'empresa',
        'idprendas',
        'nombre',
        'precio',
        'cantidad',
        'fecha',
        'tipo',
    ];

}
