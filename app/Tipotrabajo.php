<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipotrabajo extends Model
{
     protected $table = 'tipotrabajos'; //eloquent asume
    // protected $primaryKey = 'id'; //eloquent asume

    //todas las columnas q queremos ingresar y usar
    protected $fillable = ['nombre_tra', 'descripcion', 'condicion']; 

    
}
