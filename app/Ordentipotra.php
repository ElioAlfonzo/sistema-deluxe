<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordentipotra extends Model
{
    protected $table = 'orden_tipotras'; //eloquent asume

    public $timestamps = false; 
    
    protected $fillable = [
        'id_tipotrabajo',
        'id_prenda',
        'id_ordenpren',
        'estado',
        'prenda_numor',

    ];
}
