<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guiaprenda extends Model
{
    public $table = "guia_prendas";
    public $timestamps = false; 
     protected $fillable = [
        'id_guiades',
        'guiades',
        'idprenda',
        'nombre',
        'cantiprendas',
        'guia_ordent',
        'fecha',
        'guia_precio'
    ];

}
