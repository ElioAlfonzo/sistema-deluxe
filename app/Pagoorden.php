<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagoorden extends Model
{
    public $table = "pago_ordenes";
    public $timestamps = false; 

    protected $fillable = [
        'idformapago',
        'idordentrabajo',
        'idmediopago',
        'idinpecpago',
        'monto',
        'pago_fecha_hora',
        'estado',
        'pago_numor'
    ];

    public function ordentrabajo(){ //muchas prendas pertenecen a una orden de trabajo
        return $this->belongsTo('App/Ordentrabajo');
    }

}
