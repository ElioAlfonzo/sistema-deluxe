<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'roles'; //para queno tenga probema laravel al hacer referencia a la tabla
    protected $fillable = ['nombre','descripcion','condicion'];
    public $timestamps = false; //como no usamos estos campos lo ponemos en false

    
    public function users()
    {
        //Rol puede tener varios usuarios
        return $this->hasMany('App\User');    

    } 
}
