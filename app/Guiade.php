<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guiade extends Model
{
    public $table = "guia_despachos";
    public $timestamps = false; 
     protected $fillable = [
        'guia_des',
        'id_perempresa',
        'nom_perem',
        'observacion',
        'idorden',
        'guia_ordent',
        'fecha_hora',
        'fecha_entrega',
        'hora_entrega',
        'idusuario',
    ];
}
