<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mediopago extends Model
{
    public $table = "mediopagos";

    protected $fillable = [
        'nombre',
        'condicion'
    ];
}
