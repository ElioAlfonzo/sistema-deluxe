<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = "users"; 
    protected $primaryKey = 'id'; //eloquent asume
    protected $fillable = [
         'id','usuario','nombre','direccion','telefono','email','password','condicion','id_rol'
    ];
    // 'id_persona',

    //indicamos q los campos timestamps no existen
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ //campos ocultos
        'password', 'remember_token',
    ];

    //relaciones
    public function rol(){
        return $this->belongsTo('App\Rol');
    }

    // public function persona(){
    //     return $this->belongsTo('App\Persona');
    // }
}
