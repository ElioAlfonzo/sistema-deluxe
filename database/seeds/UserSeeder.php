<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'usuario' => 'elioluis',
            'nombre' => 'elioalfonzo',
            'direccion' => 'Viña',
            'telefono' => '999999',
            'email' =>'alfonzoelio7@gmail.com',
            'password' => bcrypt('20796387'),
            'condicion' => '1',
            'id_rol' => '1'
        ]);
    }
}
