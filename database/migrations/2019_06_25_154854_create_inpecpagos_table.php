<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInpecpagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inpecpagos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->boolean('estado')->default(1);
        });

        DB::table('inpecpagos')->insert(array('id' => '1','nombre'=>'Inicial'));
        DB::table('inpecpagos')->insert(array('id' => '2','nombre'=>'Final'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inpecpagos');
    }
}
