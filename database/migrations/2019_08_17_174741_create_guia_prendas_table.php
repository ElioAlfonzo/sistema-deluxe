<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuiaPrendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guia_prendas', function (Blueprint $table) {
            
            $table->increments('id');

            //Guia Despachos
            $table->integer('id_guiades')->unsigned();
            $table->foreign('id_guiades')->references('id')->on('guia_despachos');
            $table->string('guiades',15);
            //

            //Prendas
            $table->integer('idprenda');
            $table->string('nombre',50);
            $table->integer('cantiprendas');
            $table->decimal('guia_precio',11,2);
            //
            
            //
            $table->integer('idempresa');
            //
            $table->string('guia_ordent',15);
            
            $table->date('fecha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guia_prendas');
    }
}
