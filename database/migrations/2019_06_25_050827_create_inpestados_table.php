<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInpestadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inpestados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->string('descripcion', 150)->nullable(); //en caso si quiere dejarlo vacio
            $table->boolean('condicion')->default(1);
            $table->timestamps();
        });
        DB::table('inpestados')->insert(array('id' => '1','nombre'=>'Pendiente de Proceso','descripcion'=>'Cuando el cliente la entrega en el local (inicio del ciclo)'));
        DB::table('inpestados')->insert(array('id' => '2','nombre'=>'Despachado a Planta','descripcion'=>'La prenda fue procesada en la planta'));
        DB::table('inpestados')->insert(array('id' => '3','nombre'=>'Procesado','descripcion'=>'La prenda se despacho al local'));
        DB::table('inpestados')->insert(array('id' => '4','nombre'=>'Despachado a local','descripcion'=>'Ha sido enviado de la planta al local destino'));
        DB::table('inpestados')->insert(array('id' => '5','nombre'=>'Listo para entrega','descripcion'=>'Cuando la recepcionista recibio la prenda conforme en el local'));
        DB::table('inpestados')->insert(array('id' => '6','nombre'=>'Entregado','descripcion'=>'La Orden ya fue entregada al cliente'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inpestados');
    }
}
