<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuiaDespachosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guia_despachos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guia_des',15);

            //Personas o Empresa
            $table->integer('id_perempresa')->unsigned();
            $table->foreign('id_perempresa')->references('id')->on('personas');
            $table->string('nom_perem', 100);
            //

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //
            
            $table->string('observacion', 450)->nullable();

            //Orden Trabajo
            $table->integer('idorden')->unsigned();
            $table->foreign('idorden')->references('id')->on('orden_trabajos');
            
            $table->string('guia_ordent',15);
            //

            $table->dateTime('fecha_hora');

            //Modificado este campo
            $table->date('fecha_entrega');
            $table->time('hora_entrega');

            $table->boolean('estado')->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guia_despachos');
    }
}
