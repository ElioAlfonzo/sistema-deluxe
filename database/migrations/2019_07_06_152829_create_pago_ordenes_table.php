<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagoOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pago_ordenes', function (Blueprint $table) {
            $table->increments('id');

            //Forma de Pagos
            $table->integer('idformapago')->unsigned();
            $table->foreign('idformapago')->references('id')->on('formapagos');

            //Orden Trabajos
            $table->integer('idordentrabajo')->unsigned();
            $table->foreign('idordentrabajo')->references('id')->on('orden_trabajos');

            //Medio de Pago
            $table->integer('idmediopago')->unsigned();
            $table->foreign('idmediopago')->references('id')->on('mediopagos');

            //Inpeccion de pagos
            $table->integer('idinpecpago')->unsigned();
            $table->foreign('idinpecpago')->references('id')->on('inpecpagos');

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');

            $table->string('pago_numor',15);
            $table->decimal('monto', 11, 2);
            $table->dateTime('pago_fecha_hora');
            $table->boolean('estado')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pago_ordenes');
    }
}
