<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediopagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mediopagos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 70);
            $table->boolean('condicion')->default(1);
        });

        DB::table('mediopagos')->insert(array('id' => '1','nombre'=>'Efectivo'));
        DB::table('mediopagos')->insert(array('id' => '2','nombre'=>'Debito'));
        DB::table('mediopagos')->insert(array('id' => '3','nombre'=>'Credito'));
        DB::table('mediopagos')->insert(array('id' => '4','nombre'=>'No Aplica'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mediopagos');
    }
}
