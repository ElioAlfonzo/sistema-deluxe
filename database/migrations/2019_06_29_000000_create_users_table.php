<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) 
        {   
            
            $table->increments('id'); //para poder relacionar con persona
            //cremaos la FK este campo id hae referencia al campo id d la tb personas y eliminacion en cascadas
            // $table->foreign('id')->references('id')->on('personas')->onDelete('cascade');
            $table->string('usuario')->unique();//este sera el campo con el q ingresare al sistema
            $table->string('nombre', 100);
            $table->string('direccion', 70);
            $table->string('telefono', 20);
            $table->string('email', 50);

            $table->string('password'); //mi clave
            $table->boolean('condicion')->default(1);
            
            $table->integer('id_rol')->unsigned();//para poder relacionar con roles
            $table->foreign('id_rol')->references('id')->on('roles');

            $table->rememberToken();
            // $table->timestamps();
        });
        DB::table('users')->insert(array('id' => '1','usuario'=>'admin','nombre'=>'Elio','direccion'=>'1','telefono'=>'956259823','email'=>'alfonzoelio7@gmail.com','password'=>'$2y$10$eoXUEkXIrvxMgY78lGBg7eFKdiRc5lnVc0doOWHm9.MtWub/eNIxK','condicion'=>'1','id_rol'=>'1','remember_token'=>NULL));  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
