<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriaOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historia_ordenes', function (Blueprint $table) {
            $table->increments('id');

            //Ordenes de Trabajo
            $table->integer('idorden')->unsigned();
            $table->foreign('idorden')->references('id')->on('orden_trabajos');

            //Inpeccion de estados
            $table->integer('idinpes')->unsigned();
            $table->foreign('idinpes')->references('id')->on('inpestados');

            //Users
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');

            

            //Fecha
            $table->date('fecha_hora');
            $table->date('entrega');
            $table->string('histo_numor',15);
            
            $table->string('tipo', 20)->nullable(); //en caso si quiere dejarlo vacio;;

            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historia_ordenes');
    }
}
