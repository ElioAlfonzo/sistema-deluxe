<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormapagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formapagos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->boolean('estado')->default(1);
        });
         DB::table('formapagos')->insert(array('id' => '1','nombre'=>'Completo'));
         DB::table('formapagos')->insert(array('id' => '2','nombre'=>'Sin Pago'));
         DB::table('formapagos')->insert(array('id' => '3','nombre'=>'Abono'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formapagos');
    }
}
