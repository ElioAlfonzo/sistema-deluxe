<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadoPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado_pago', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folio');
            $table->date('fecha1');
            $table->date('fecha2');
            $table->date('fechaorigen');
            $table->string('idempresa',15)->nullable(); //en caso si quiere dejarlo vacio;
            $table->string('empresa',100)->nullable(); //en caso si quiere dejarlo vacio;
            $table->decimal('neto', 11, 2)->nullable(); //en caso si quiere dejarlo vacio;;
            $table->decimal('iva', 11, 2)->nullable(); //en caso si quiere dejarlo vacio;;
            $table->decimal('total', 11, 2)->nullable(); //en caso si quiere dejarlo vacio;;
            // $table->integer('status')->nullable(); //en caso si quiere dejarlo vacio;;;
            $table->boolean('status')->default(1);

            $table->integer('sii')->nullable(); //en caso si quiere dejarlo vacio;;;
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estado_pago');
    }
}
