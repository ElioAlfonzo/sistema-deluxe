<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_trabajos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_orden',15);

            //Personas
            $table->integer('idpersona')->unsigned();
            $table->foreign('idpersona')->references('id')->on('personas');
            $table->string('nombre', 100);
            //

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->string('observacion', 450)->nullable();
            $table->dateTime('fecha_hora');

            $table->date('fechai');
            $table->time('horai');
            
            $table->date('fecha_entrega');
            $table->time('hora_entrega');

            //Ubicaciones
            $table->integer('idubicacion')->unsigned();
            $table->foreign('idubicacion')->references('id')->on('ubicaciones');

            //Inpeccion de estados
            $table->integer('idinpes')->unsigned();
            $table->foreign('idinpes')->references('id')->on('inpestados');
            
            $table->decimal('total_orden', 11, 2);
            // este estado debe ser para ver si ya fue despachada o no la orden
            $table->boolean('estado')->default(1);

            // Este vaa controlar si se pago completo la orden o no
            $table->boolean('estado_pago')->default(0);

            $table->boolean('estadoinven')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_trabajos');
    }
}
