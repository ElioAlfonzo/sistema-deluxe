<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenPrendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_prendas', function (Blueprint $table) {
            $table->increments('id');


            // //Prendas
            $table->integer('idprendas');

            // //Tipo trabajo
            // $table->integer('idtipotrabajo')->unsigned();
            // $table->foreign('idtipotrabajo')->references('id')->on('tipotrabajos');

            
            //Orden de Trabajo
            $table->integer('idordentra')->unsigned();
            $table->foreign('idordentra')->references('id')->on('orden_trabajos');
            $table->string('prenda_numor',15);

             //Inpeccion de estados
             $table->integer('idinpes')->unsigned();
             $table->foreign('idinpes')->references('id')->on('inpestados');

            $table->decimal('precio', 11, 2);
            $table->integer('cantiprendas');
            $table->integer('stockprendas');
            $table->boolean('condicion')->default(1);
            $table->date('fecha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_prendas');
    }
}
