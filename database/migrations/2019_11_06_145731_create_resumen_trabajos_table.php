<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumenTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumen_trabajos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ordent',15)->nullable(); //en caso si quiere dejarlo vacio;
            $table->string('idempresa',15)->nullable(); //en caso si quiere dejarlo vacio;
            $table->string('empresa',100)->nullable(); //nombre de la empresa le modifique el valor de 15 a 100
            $table->string('guiades',15)->nullable(); //en caso si quiere dejarlo vacio;
            $table->integer('idprendas');
            $table->string('nombre',50)->nullable(); //en caso si quiere dejarlo vacio;;
            $table->decimal('precio', 11, 2)->nullable(); //en caso si quiere dejarlo vacio;;
            $table->integer('cantidad');
            $table->date('fecha');
            $table->integer('tipo'); //para identificar si viene de una guia o es solo del todas las prendas que tiene contratada el cliente
            $table->boolean('status')->default(1);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumen_trabajos');
    }
}
