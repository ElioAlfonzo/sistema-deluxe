<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrenPreciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pren_precios', function (Blueprint $table) {
            $table->increments('id');
            // //Prendas
            $table->integer('idprendas')->unsigned();
            $table->foreign('idprendas')->references('id')->on('prendaempresas');

            // //Empresas
            $table->integer('idempresa')->unsigned();
            $table->foreign('idempresa')->references('id')->on('personas');
            
            $table->decimal('precio', 11, 2);
            $table->boolean('condicion')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pren_precios');
    }
}
