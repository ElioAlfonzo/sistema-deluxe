<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadopagosPrendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estadopagos_prendas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idprendas');
            $table->string('nombre',50)->nullable(); //en caso si quiere dejarlo vacio;;
            $table->string('idempresa',15)->nullable(); //en caso si quiere dejarlo vacio;
            $table->string('empresa',100)->nullable(); //en caso si quiere dejarlo vacio;
            $table->decimal('precio', 11, 2)->nullable(); //en caso si quiere dejarlo vacio;;
            $table->integer('cantidad');
            $table->date('fecha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estadopagos_prendas');
    }
}
