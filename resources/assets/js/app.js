/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
window.Vue = require('vue');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('tipotrabajo', require('./components/Tipotrabajo.vue'));
Vue.component('prenda', require('./components/Prenda.vue'));
Vue.component('prendaempresa', require('./components/Prendaempresa.vue'));
Vue.component('inpestado', require('./components/Inpestado.vue'));
Vue.component('ubicacion', require('./components/Ubicacion.vue'));
Vue.component('cliente', require('./components/Cliente.vue'));
Vue.component('clientempresa', require('./components/ClienteEmpresa.vue'));
Vue.component('rol', require('./components/Rol.vue'));
Vue.component('user', require('./components/User.vue'));
Vue.component('trabajo', require('./components/Trabajo.vue'));
Vue.component('trabajoempre', require('./components/TrabajoEmpre.vue'));
Vue.component('caja', require('./components/Caja.vue'));
Vue.component('inventario', require('./components/Inventario.vue'));
Vue.component('movimiento', require('./components/Movimiento.vue'));
Vue.component('dashboard', require('./components/Dashboard.vue'));
Vue.component('dashboard2', require('./components/Dashboard2.vue'));
Vue.component('despacho', require('./components/Despacho.vue'));
Vue.component('despachocli', require('./components/DespachoCli.vue'));
Vue.component('estadopago', require('./components/EstadoPago.vue'));

const app = new Vue({
    el: '#app',
    data: {
        menu: 0,
        ruta: ''
    }
});