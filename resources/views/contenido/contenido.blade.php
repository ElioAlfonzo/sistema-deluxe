@extends('principal')

@section('contenido')
    
    @if(Auth::check()) <!--Si el usuario actual esta autenticado-->

        @if(Auth::user()->id_rol == 1) <!--Administrador-->
                    <!-- @include('plantilla.sidebaradministrador') -->
                    <template v-if="menu==0">
                        <dashboard :ruta="ruta"></dashboard>
                    </template>

                    <template v-if="menu==2">
                        <h1>Contenido Menu 2</h1>
                    </template>

                    <template v-if="menu==3">
                        <tipotrabajo :ruta="ruta"></tipotrabajo>
                    </template>
                    
                    <template v-if="menu==4">
                        <prenda :ruta="ruta"></prenda>
                    </template>

                    <template v-if="menu==5">
                        <inpestado :ruta="ruta"></inpestado>
                    </template>

                    <template v-if="menu==6">
                        <ubicacion :ruta="ruta"></ubicacion>
                    </template>

                    <template v-if="menu==7">
                        <cliente :ruta="ruta"></cliente>
                    </template>

                    <template v-if="menu==8">
                        <rol :ruta="ruta"></rol>
                    </template>

                    <template v-if="menu==9">
                        <user :ruta="ruta"></user>
                    </template>

                    <template v-if="menu==10">
                        <despacho :ruta="ruta"></despacho>
                    </template>

                    <template v-if="menu==11">
                        <trabajo :ruta="ruta"></trabajo>
                    </template>

                    <template v-if="menu==12">
                        <clientempresa :ruta="ruta"></clientempresa>
                    </template>

                    <template v-if="menu==13">
                        <caja :ruta="ruta"></caja>
                    </template>
                    
                    <template v-if="menu==14">
                        <inventario :ruta="ruta"></inventario>
                    </template>

                    <template v-if="menu==15">
                        <movimiento :ruta="ruta"></movimiento>
                    </template>

                    <template v-if="menu==16">
                        <trabajoempre :ruta="ruta"></trabajoempre>
                    </template>

                    <template v-if="menu==17">
                        <estadopago :ruta="ruta"></estadopago>
                    </template>

                    <template v-if="menu==18">
                        <despachocli :ruta="ruta"></despachocli>
                    </template>

                    <template v-if="menu==19">
                        <prendaempresa :ruta="ruta"></prendaempresa>
                    </template>
        

                    <template v-if="menu==20">
                        <dashboard :ruta="ruta"></dashboard>
                    </template>

                    
                @elseif (Auth::user()->id_rol == 2) <!--Recepcionista-->
                    <template v-if="menu==0">
                        <dashboard2 :ruta="ruta"></dashboard2>
                    </template>

                    <template v-if="menu==4">
                        <prenda :ruta="ruta"></prenda>
                    </template>

                    <template v-if="menu==7">
                        <cliente :ruta="ruta"></cliente>
                    </template>

                    <template v-if="menu==12">
                        <clientempresa :ruta="ruta"></clientempresa>
                    </template>


                    <template v-if="menu==18">
                        <despachocli :ruta="ruta"></despachocli>
                    </template>

                    <template v-if="menu==11">
                        <trabajo :ruta="ruta"></trabajo>
                    </template>

                    <template v-if="menu==13">
                        <caja :ruta="ruta"></caja>
                    </template>

                    <template v-if="menu==14">
                        <inventario :ruta="ruta"></inventario>
                    </template>

                    <template v-if="menu==15">
                        <movimiento :ruta="ruta"></movimiento>
                    </template>
                    
                    <template v-if="menu==17">
                        <estadopago :ruta="ruta"></estadopago>
                    </template>

                    <template v-if="menu==19">
                        <prendaempresa :ruta="ruta"></prendaempresa>
                    </template>
            
                @elseif (Auth::user()->id_rol == 3) <!--Operador-->

                <template v-if="menu==0">
                    <dashboard2 :ruta="ruta"></dashboard2>
                </template>

                <template v-if="menu==10">
                    <despacho :ruta="ruta"></despacho>
                </template>
                
                <template v-if="menu==16">
                    <trabajoempre :ruta="ruta"></trabajoempre>
                </template>

                <template v-if="menu==15">
                    <movimiento :ruta="ruta"></movimiento>
                </template>
                
                <template v-if="menu==17">
                    <estadopago :ruta="ruta"></estadopago>
                </template>

        @else

        @endif

    @endif


    
@endsection