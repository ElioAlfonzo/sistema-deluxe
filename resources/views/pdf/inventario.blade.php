<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Reporte de Prendas-Pdf</title>
    <style>
    
        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
            font-size: 0.870rem;
            font-weight: normal;
            line-height: 1;
            color: #151b1e;           
        }
        .table {
            display: table;
            width: 100%;
            max-width: 100%;
            margin-bottom: 0rem;
            background-color: transparent;
            border-collapse: collapse;
        }
        .table-bordered {
            border: 1px solid #c2cfd6;
        }
        thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }
        tr {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }
        .table th, .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #c2cfd6;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #c2cfd6;
        }
        .table-bordered thead th, .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-bordered th, .table-bordered td {
            border: 1px solid #c2cfd6;
        }
        th, td {
            display: table-cell;
            vertical-align: inherit;
        }
        th {
            font-weight: bold;
            text-align: -internal-center;
            text-align: left;
        }
        tbody {
            display: table-row-group;
            vertical-align: middle;
            border-color: inherit;
        }
        tr {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }
        .table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(0, 0, 0, 0.05);
        }
        .izquierda{
            float:left;
        }
        .derecha{
            float:right;
        }

    </style>
</head>
<body>
    <div>
        <h3>Lista de Prendas <span class="derecha">{{date("d-m-Y")}}</span> </h3>
    </div>


    <div>
        <table class="table table-bordered table-striped table-sm">
            <thead>
                <tr>
                    <th>Orden</th>
                    <th>Cant</th>
                    <th>Prenda</th>
                    <th>Estado</th>
                    <th>Fecha</th>
                    <th>Entrega</th>
                    
                    
                </tr>
            </thead>
            <!-- class="badge badge-danger" class="badge badge-success"-->
            <tbody>
                @foreach ($prendas as $p)
                <tr>
                    
                    <td>{{$p->idordentra}}</td>
                    <td>{{$p->cantiprendas}}</td>
                    <td>{{$p->prenda}}</td>
                    <td>{{$p->estado}}</td>
                    <td>{{$p->fechayhora}}</td>
                    <td>{{$p->fechaen}}</td>
                </tr>
                @endforeach
                
            </tbody>
        </table>                         
    </div>
    
    <div class="izquierda">
        <p><strong>Total de Registros:</strong></p>
    </div>
</body>
</html>