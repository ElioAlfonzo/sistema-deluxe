<!DOCTYPE>
<html>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Orden de Trabajo N°</title>
    <style>
        body {
        /*position: relative;*/
        /*width: 16cm;  */
        /*height: 29.7cm; */
        /*margin: 0 auto; */
        /*color: #555555;*/
        /*background: #FFFFFF; */
        font-family: Arial, sans-serif; 
        font-size: 14px;
        /*font-family: SourceSansPro;*/
        }

        #logo{
        float: left;
        margin-top: 0%;
        margin-left: 2%;
        margin-right: 2%;
        background: #2183E3;
        padding: 5px;
        }

        #imagen{
        width: 100px;
        }

        #datos{
        float: left;
        margin-top: 0%;
        margin-left: 2%;
        margin-right: 2%;
        /*text-align: justify;*/
        }

        #encabezado{
        text-align: center;
        margin-left: 10%;
        margin-right: 35%;
        font-size: 15px;
        }

        #fact{
        /*position: relative;*/
        float: right;
        margin-top: 2%;
        margin-left: 2%;
        margin-right: 2%;
        font-size: 20px;
        }

        section{
        clear: left;
        }

        #cliente{
        text-align: left;
        }

        #facliente{
        width: 40%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }

        #fac, #fv, #fa{
        color: #FFFFFF;
        font-size: 15px;
        }

        #facliente thead{
        padding: 20px;
        background: #2183E3;
        text-align: left;
        border-bottom: 1px solid #FFFFFF;  
        }

        #facvendedor{
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }

        #facvendedor thead{
        padding: 20px;
        background: #2183E3;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;  
        }

        #facarticulo{
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }

        #facarticulo thead{
        padding: 20px;
        background: #2183E3;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;  
        }

        #gracias{
        text-align: center; 
        }

        #centrar{
            text-align: center; 
        }
        #derecha{
            text-align: right; 
        }
        
        
    </style>
    <body>
        @foreach ($trabajo as $t)
        <header>
            <div id="logo">
                <img src="img/logo-1.png" alt="incanatoIT" id="imagen">
            </div>
            <div id="datos">
                <p id="encabezado">
                    <b>DELUXE LIMITADA</b><br><b>Lavanderia Industrial</b><br><b>RUT:76.315.694-K</b><br> C.M: Jimenez N° 71 - Limache <br>Web: www.serviciosdeluxe.cl<br>Email: ventas@serviciosdeluxe.cl<br> Telefono:(+51)931742904<br>
                </p>
            </div>
            <div id="fact">
                <p>N° Orden<br>
                    {{$t->num_orden}}</p>
            </div>
        </header>
        <br>
        <section>
            <div>
                <table id="facliente">
                    <thead>                        
                        <tr>
                            <th id="fac">Cliente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th><p id="cliente">Sr(a). {{$t->nombre}}<br>
                            Cod Persona: {{$t->id_cod_persona}}<br>
                            Dirección: {{$t->direccion}}<br>
                            Teléfono: {{$t->telefono}}<br>
                            Email: {{$t->email}}</</p></th>
                            <p>Estado: {{$estadopago[0]->nombre}}</p>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        
        <br>
        <section>
            <div>
            <table id="facvendedor">
                    <thead>
                        <tr id="fv">
                            <th>Fecha de Orden</th>
                            <th>Fecha de Entrega</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="centrar">{{$entregado[0]->pago_fecha_hora}}</td>
                            <td id="centrar">{{$t->fecha_entrega}}</td>
                        </tr>
                    </tbody>
            </table>

            </div>
        </section>
        

        @endforeach
        <br>
        <section>
            <div>
                <table id="facvendedor">
                    <thead>
                        <tr id="fv">
                            <th>VENDEDOR</th>
                            <th>LOCAL</th>
                            <th>Medio P.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="centrar">{{$t->nomusu}}</td>
                            <td id="centrar">{{$local[0]->local}}</td>
                            <td id="centrar">{{$medio[0]->nombre}}</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </section>
        <br>
        <section>
            <div>
                <table id="facarticulo">
                    <thead>
                        <tr id="fa">
                            <th>CANT</th>
                            <th>DESCRIPCION</th>
                            <th>PRECIO UNIT</th>
                            <th>PRECIO TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($prenda as $det)
                        <tr>
                            <td id="centrar">{{$det->cantiprendas}}</td>
                            <td id="centrar">{{$det->prenda}}</td>
                            <td id="centrar">{{$det->precio}}</td>
                            <td id="derecha">{{$det->cantiprendas*$det->precio}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        @foreach ($trabajo as $t)
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            
                            <th></th>
                            <td><strong>Pago:</strong>{{$pago[0]->nombre}}</td>
                            <td> {{$montoegre[0]->monto}}</td>
                            
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Primer Pago</th>
                            <td>$ {{$t->total_orden - $montoegre[0]->monto}}</td>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>TOTAL</th>
                            <td>$ {{$t->total_orden}}</td>
                        </tr>
                        @endforeach
                    </tfoot>
                </table>
            </div>
        </section>
        <br>
        <br>
        <footer>
            <div id="gracias">
                <p><b>Gracias por su Confianza!</b></p>
            </div>
        </footer>
    </body>
</html>