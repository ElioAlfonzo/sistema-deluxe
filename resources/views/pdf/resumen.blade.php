<!DOCTYPE>
<html>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Estado de Pago</title>
    <style>
        .right {
        float: right;
        }

        .txright {
            text-align: right;
        }
        .txcenter {
            text-align: center;
        }
        .border {
        border: 2px solid lightblue;
        }
        /* Titulo */
        #customers1 {
        border-collapse: collapse;
        width: 100%;
        }


        #customers1 th {
        padding-top: 10px;
        padding-bottom: 10px;
        /* padding: 10px; */
        text-align: center;
        color: black;
        }

        #customers1 td,
        #customers1 th {
            border: 1.5px solid #373737;
            /* padding: 15px; */
            font-size: 10px;
        }

        /* 1 */
        #customers {
        border-collapse: collapse;
        width: 100%;
        }


        #customers th {
        padding-top: 8px;
        padding-bottom: 8px;
        text-align: center;
        background-color: #ececec;
        color: black;
        }

        #customers td,
        #customers th {
            border: 1.5px solid #373737;
            font-size: 7px;
        }

        /*2*/
         /* Otros */
         #customers2 {
        border-collapse: collapse;
        width: 100%;
        }


        #customers2 th {
        padding-top: 10px;
        padding-bottom: 10px;
        text-align: center;
        background-color: #C0C0C0;
        color: black;
        }
        #customers2 td {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        color: black;
        }

        #customers2 td,
        #customers2 th {
            border: 1.5px solid #373737;
            font-size: 7px;
        }

        #col1 {
            width:30%;
        }

        #col2 {
            width:70%;
        }

        #logo{
        display: block;;
        
        /* background: #2183E3; */
        /* width: 80%; */
        
        }
        #imagen{
            width: 140px;
            height: 65px;
        }

        .decoracion {
            background-color: #ececec;
        }
        .decoracion2 {
            background-color: #C0C0C0;
        }
    </style>
    <head>

    <table id="customers1">
            <tr>
            
            <th id="col1">
                <div id="logo">
                    <img src="img/logodeluxe-02.jpg" alt="" id="imagen">
                </div>
            </th>
            <th id="col2">ESTADO DE PAGO</th>
            </tr>
        
    </table>
    </head>
    <body>
        <table id="customers">
            <thead>
                <tr>
                    <th>Numero</th>
                    <th>Codigo</th>
                    <th>Revision</th>
                    <th>Fecha</th>
                    <th>Pagina</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="txcenter">{{$folio}}</td>
                    <td class="txcenter">#</td>
                    <td class="txcenter">#</td>
                    <td class="txcenter">{{$hoy}}</td>
                    <td class="txcenter">#</td>
                </tr>
            </tbody>
        </table>
        
        <table class="mt-3" id="customers">
            
                <tr>
                    <td class="txcenter decoracion">MES</td>
                    <td class="txcenter decoracion">{{$mes}}</td>
                    <td class="txcenter decoracion">NOMBRE S.</td>
                    <td class="txcenter decoracion">{{$empresa[0]->nombre}}</td>
                </tr>
                <tr>
                    <td class="txcenter">DESDE</td>
                    <td class="txcenter">{{$ini}}</td>
                    <td class="txcenter">NETO</td>
                    <td class="txcenter">$ {{number_format($toto,2)}}</td>
                </tr>
                <tr>
                    <td class="txcenter decoracion">HASTA</td>
                    <td class="txcenter decoracion">{{$fin}}</td>
                    <td class="txcenter decoracion">IVA</td>
                    
                    <td class="txcenter decoracion">$ {{number_format($iva,2)}}</td>
                </tr>
                <tr>
                    <td class="txcenter">FECHA R.</td>
                    <td class="txcenter">#</td>
                    <td class="txcenter">TOTAL</td>
                    <td class="txcenter">$ {{number_format($totalfinal,2)}}</td>
                </tr>
        </table>

        <table class="mt-3" id="customers2">
            <thead>
                <tr>
                    <th>Prendas / Precio</th>
                    @foreach ($fechas as $fe)
                    <th>{{$fe->fecha}}</th>
                    @endforeach
                    <th>Control Lav.</th>
                    <th>Total</th>
                </tr>
            </thead>

            <tbody>
                    

                @foreach ($pren as $pe)        
                    <tr>
                        <td class="decoracion2">{{$pe->nombre}} / {{$pe->precio}}</td>
                        
                        @foreach ($fechas as $fe)
                        <td>

                        @foreach ($guiap as $gui)    
                                @if($gui->fecha == $fe->fecha && $gui->idprendas == $pe->idprendas)
                                {{$gui->cantidad}}
                                @endif
                        @endforeach

                        </td>
                        @endforeach

                        <td>
                        @foreach ($totalfecha as $totafe)
                            @if($pe->idprendas == $totafe->idprendas)
                                {{$totafe->cantidad}}
                            @endif
                        @endforeach
                        </td>

                        
                        <td class="decoracion2">
                        @foreach ($totales as $to)
                            @if($pe->idprendas == $to->idprendas)
                                $ {{number_format ($to->total)}}
                            @endif
                        @endforeach
                        </td>
                        
                    </tr>
                @endforeach

                <tr>
                    <td class="decoracion2">Total N°</td>
                    @foreach ($prueba as $prueb)
                        <td>{{$prueb->total}}</td>
                    @endforeach


                    <td>
                    {{$suma}}
                    </td>

                    <td class="decoracion2">=</td>
                </tr>

                

                <tr>
                    <td class="decoracion2">Total $</td>
                    @foreach ($fechas as $fe)
                    <td>
                        <!-- recorreme los resultados y los sumamos  -->
                        <?php $valor = 0;    ?>
                        @foreach($pren2 as $pepe)
                                @if($pepe->fecha == $fe->fecha)
                                <?php $valor += $pepe->cantidad ?>
                                @endif        
                        @endforeach

                        <!-- Luego fuera del for imprimimos el total -->
                        $ {{number_format($valor,2)}}
                    </td>
                    @endforeach
                    
                    <td colspan="2" class="text-center decoracion2">$ {{number_format($toto,2)}}</td>
                </tr>
            
            </tbody>
        </table>
    </body>
</html>
