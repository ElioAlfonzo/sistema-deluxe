<!DOCTYPE>
<html>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Orden Despacho N°</title>
    <style>
        body {
        /*position: relative;*/
        /*width: 16cm;  */
        /*height: 29.7cm; */
        /*margin: 0 auto; */
        /*color: #555555;*/
        /*background: #FFFFFF; */
        font-family: Arial, sans-serif; 
        font-size: 14px;
        /*font-family: SourceSansPro;*/
        }
        #logo{
        float: left;
        margin-top: 0%;
        margin-left: 2%;
        margin-right: 2%;
        background: #2183E3;
        padding: 5px;
        }
        #imagen{
        width: 100px;
        }
        #datos{
        float: left;
        margin-top: 0%;
        margin-left: 2%;
        margin-right: 2%;
        /*text-align: justify;*/
        }
        #encabezado{
        text-align: center;
        margin-left: 10%;
        margin-right: 35%;
        font-size: 15px;
        }
        #fact{
        /*position: relative;*/
        float: right;
        margin-top: 2%;
        margin-left: 2%;
        margin-right: 2%;
        font-size: 20px;
        }
        section{
        clear: left;
        }
        #cliente{
        text-align: left;
        }
        #facliente{
        width: 40%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }

        #fac, #fv, #fa{
        color: #FFFFFF;
        font-size: 15px;
        }

        #facliente thead{
        padding: 20px;
        background: #2183E3;
        text-align: left;
        border-bottom: 1px solid #FFFFFF;  
        }

        #facvendedor{
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }

        #facvendedor thead{
        padding: 20px;
        background: #2183E3;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;  
        }

        #facarticulo{
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }

        #facarticulo thead{
        padding: 20px;
        background: #2183E3;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;  
        }

        #observacion tfoot{
        padding: 20px;
        background: #2183E3;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;  
        }

        #gracias{
        text-align: center; 
        }

        #centrar{
            text-align: center; 
        }
        #derecha{
            text-align: right; 
        }
        
        
    </style>
    <body>
        @foreach ($trabajo as $t)
        <header>
            <div id="logo">
                <img src="img/logo-1.png" alt="incanatoIT" id="imagen">
            </div>
            <div id="datos">
                <p id="encabezado">
                    <b>DELUXE LIMITADA</b><br><b>Lavanderia Industrial</b><br><b>RUT:76.315.694-K</b><br> C.M: Jimenez N° 71 - Limache <br>Web: www.serviciosdeluxe.cl<br>Email: ventas@serviciosdeluxe.cl<br> Telefono:(+51)931742904<br>
                </p>
            </div>
            <div id="fact">
                <p>N° Despa<br>
                    {{$despa[0]->guia_des}}</p>
                    
            </div>
        </header>
        <br>
        <section>
            <div>
                <table id="facliente">
                    <thead>                        
                        <tr>
                            <th id="fac">Cliente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th><p id="cliente">Sr(a). {{$t->nombre}}<br>
                            Cod Persona: {{$t->id_cod_persona}}<br>
                            Dirección: {{$t->direccion}}<br>
                            Teléfono: {{$t->telefono}}<br>
                            Email: {{$t->email}}</</p></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        
        <br>
        <section>
            <div>
            <table id="facvendedor">
                    <thead>
                        <tr id="fv">
                            <th>Fecha de Despacho</th>
                            <th>Fecha de Entrega</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="centrar">{{$t->fecha_hora}}</td>
                            <td id="centrar">{{$t->fecha_entrega}}</td>
                        </tr>
                    </tbody>
            </table>

            </div>
        </section>
        

        @endforeach
        <br>
        <section>
            <div>
                <table id="facarticulo">
                    <thead>
                        <tr id="fa">
                            <th>CANT</th>
                            <th>DESCRIPCION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($prenda as $det)
                        <tr>
                            <td id="centrar">{{$det->cantiprendas}}</td>
                            <td id="centrar">{{$det->prenda}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
        
        <section>
            <div>
                <table id="facarticulo">
                    <thead>
                        <tr id="fa">
                            <th>
                                OBSERVACION
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($trabajo as $t)
                        <tr>
                            <td>{{$t->observacion}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
        <br>
        <br>
        <footer>
            <div id="gracias">
                <p><b>Gracias por su Confianza!</b></p>
            </div>
        </footer>
    </body>
</html>