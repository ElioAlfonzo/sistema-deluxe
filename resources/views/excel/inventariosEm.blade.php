
   <html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte de Inventario-Excel</title>
    
</head>
    <body>
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Cliente</th>
                        <th>Orden</th>
                        <th>Cantidad</th> 
                        <th>Stock</th> 
                        <th>Prendas</th>  
                        <th>Estado</th>  
                        <th>Fecha Recibo</th>         
                        <th>Fecha Entrega</th>
                        <th>Local</th>
                        

                    </tr>
                </thead>
                <tbody>
                @foreach ($historias as $histo)
                    <tr>
                        <td>{{$histo->nombre}}</td>
                        <td>{{$histo->idorden}}</td>
                        <td>{{$histo->cantiprendas}}</td>
                        <td>{{$histo->stockprendas}}</td>
                        @foreach ($prendas as $prenda)
                            @if($prenda->id == $histo->idprendas)
                                <td>{{$prenda->nombre}}</td>
                            @endif
                        @endforeach

                        <td>{{$histo->estados}}</td>
                        <td>{{$histo->recibo}}</td>
                        <td>{{$histo->entrega}}</td>

                        @foreach ($ubicaciones as $ubicacion)
                            @if($ubicacion->id == $histo->idubicacion)
                            <td>{{$ubicacion->local}}</td>
                            @endif
                        @endforeach
                        
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

    </body>                            
                            