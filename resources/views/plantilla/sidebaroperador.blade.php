<div class="sidebar">
                <nav class="sidebar-nav">
                    <ul class="nav">
                        <li @click="menu=0" class="nav-item">
                            <a class="nav-link active" href="#"><i class="icon-speedometer"></i> Escritorio</a>
                        </li>

                        <li class="nav-title">
                            Modulos del Sistema
                        </li>
                        
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-basket"></i>Trabajos/Empresas</a>

                            <ul class="nav-dropdown-items">
                                <li @click="menu=16" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-basket-loaded"></i>Trabajos-Ingresos</a>
                                </li>

                                <li @click="menu=10" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-basket-loaded"></i>Despachos Trabajo</a>
                                </li>
                                
                                <li @click="menu=17" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-notebook"></i>Estado de Pagos</a>
                                </li>

                            </ul>
                        </li>

                        <li @click="menu=15" class="nav-item">
                            <a class="nav-link" href="#"><i class="icon-notebook"></i>Moviemiento Ordenes</a>
                        </li>

                    </ul>
                </nav>

            <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>