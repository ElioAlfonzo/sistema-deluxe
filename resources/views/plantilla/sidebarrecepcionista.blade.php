<div class="sidebar">
                <nav class="sidebar-nav">
                    <ul class="nav">
                        <li @click="menu=0" class="nav-item">
                            <a class="nav-link active" href="#"><i class="icon-speedometer"></i> Escritorio</a>
                        </li>

                        <li class="nav-title">
                            Modulos del Sistema
                        </li>


                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-bag"></i> Almacén</a>

                            <ul class="nav-dropdown-items">

                                <li @click="menu=7" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-notebook"></i> Clientes</a>
                                </li>

                                
                                <li @click="menu=12" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-notebook"></i> Cliente Empresa</a>
                                </li>

                                <li @click="menu=4" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-bag"></i>Prendas Clientes</a>
                                </li>

                                <li @click="menu=19" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-bag"></i>Prendas Empresas</a>
                                </li>

                            </ul>

                        </li>

                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-basket"></i>Trabajo/Cliente</a>

                            <ul class="nav-dropdown-items">
                                <li @click="menu=11" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-basket-loaded"></i>Trabajos-Ingresos</a>
                                </li>
                                
                                <li @click="menu=18" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-basket-loaded"></i>Despachos Trabajo</a>
                                </li>

                            </ul>
                        </li>


                        <li @click="menu=15" class="nav-item">
                            <a class="nav-link" href="#"><i class="icon-notebook"></i>Moviemiento Ordenes</a>
                        </li>


                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-wallet"></i> Caja </a>

                            <ul class="nav-dropdown-items">
                                <li @click="menu=13" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-wallet"></i>Ver Detalles</a>
                                </li>

                            </ul>
                        </li> 


                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-pie-chart"></i> Reportes</a>

                            <ul class="nav-dropdown-items">
                                <li @click="menu=14" class="nav-item">
                                    <a class="nav-link" href="#"><i class="icon-chart"></i> Reporte de Inventario</a>
                                </li>
                                
                            </ul>
                        </li>

                    </ul>
                </nav>
                
                <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>