<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['guest']], function () { //para los q no se han autenticado, acceso a:
    //Todas las rutas a las que pueden acceder lo que no estan autenticado Login y usuarios
    Route::get('/','Auth\LoginController@showLoginForm');
    Route::post('/login','Auth\LoginController@login')->name('login');
     
});

Route::group(['middleware' => ['auth']], function () { //acceso a estas rutas para los q estan autenticado

    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/dashboard/anterior', 'DashboardController@anterior');
    Route::get('/dashboard/pendientes', 'DashboardController@pendientes');
    Route::get('/dashboard/ingdes/{fecha}', 'DashboardController@ingdes');

    Route::get('/main', function () { //aqui agregamos el main para q funcione login con /
        return view('contenido/contenido');
    })->name('main');//alias

    //SubConjunto de Middleware
    Route::group(['middleware' => ['Administrador']], function () {
        //Todas las rutas a la que puede acceder el administrador

        //************Rutas de Tipo de Trabajos******************/
        Route::get('/tipotrabajo', 'TipotrabajoController@index'); // para tomar datos de la bd
        Route::post('/tipotrabajo/registrar', 'TipotrabajoController@store'); // para registrar en la bd
        Route::put('/tipotrabajo/actualizar', 'TipotrabajoController@update'); // para actualizar en la bd
        Route::put('/tipotrabajo/desactivar', 'TipotrabajoController@desactivar'); // para desactivar en la bd
        Route::put('/tipotrabajo/activar', 'TipotrabajoController@activar'); // para actualizar en la bd
        Route::get('/tipotrabajo/listarTipotrabajo', 'TipotrabajoController@listarTipotrabajo'); //para listar la prendas desde el input de orden trabajo
        
        //************Rutas de Prendas******************/
        Route::get('/prenda', 'PrendaController@index'); // para tomar datos de la bd
        Route::post('/prenda/registrar', 'PrendaController@store'); // para registrar en la bd
        Route::put('/prenda/actualizar', 'PrendaController@update'); // para actualizar en la bd
        Route::put('/prenda/desactivar', 'PrendaController@desactivar'); // para desactivar en la bd
        Route::put('/prenda/activar', 'PrendaController@activar'); // para actualizar en la bd
        Route::get('/prenda/buscarPrenda', 'PrendaController@buscarPrenda'); //para buscar la prendas desde el input de orden trabajo
        Route::get('/prenda/listarPrenda', 'PrendaController@listarPrenda'); //para listar la prendas desde el input de orden trabajo
        Route::get('/prenda/listarPdf', 'PrendaController@listarPdf'); //para listar la prendas desde el input de orden trabajo

        //************Rutas de Prendas Empresas******************/
        Route::get('/prendaempresa', 'PrendaController@indexPren'); // para tomar datos de la bd
        Route::post('/prendaempresa/registrar', 'PrendaController@storePren'); // para registrar en la bd
        Route::put('/prendaempresa/actualizar', 'PrendaController@updatePren'); // para actualizar en la bd
        Route::put('/prendaempresa/desactivar', 'PrendaController@desactivarPren'); // para desactivar en la bd
        Route::put('/prendaempresa/activar', 'PrendaController@activarPren'); // para actualizar en la bd
        Route::get('/prendaempresa/buscarPrenda', 'PrendaController@buscarPrendaEm'); //para buscar la prendas desde el input de orden trabajo
        Route::get('/prendaempresa/listarprenda', 'PrendaController@listarPrendaEm'); //para listar la prendas desde el input de orden trabajo
        Route::get('/prendaempresa/listarPdf', 'PrendaController@listarPdfEm'); //para listar la prendas desde el input de orden trabajo

        Route::post('/prendaempresa/precios', 'PrendaController@storePrecio'); // para registrar en la bd
        Route::put('/prendaempresa/eliminar', 'PrendaController@eliminarPrecio'); // para Actualizar en las prendas de empresas
        Route::put('/prendaempresa/actualizarprecio', 'PrendaController@cambiarPrecio'); // para Actualizar en las prendas de empresas
        Route::get('/prendaempresa/prenda', 'PrendaController@verPrenda'); // para registrar en la bd
        Route::get('/prendaempresa/listarprendaprecio', 'PrendaController@listarPrendaPrecio'); //para listar la prendas desde el input de orden trabajo
        
        //************Rutas de Inspeccion de Estados******************/
        Route::get('/inpestado', 'InpestadoController@index'); // para tomar datos de la bd
        Route::post('/inpestado/registrar', 'InpestadoController@store'); // para registrar en la bd
        Route::put('/inpestado/actualizar', 'InpestadoController@update'); // para actualizar en la bd
        Route::put('/inpestado/desactivar', 'InpestadoController@desactivar'); // para desactivar en la bd
        Route::put('/inpestado/activar', 'InpestadoController@activar'); // para actualizar en la bd
        
        //************Rutas de Ubicaciones******************/
        Route::get('/ubicacion', 'UbicacionController@index'); // para tomar datos de la bd
        Route::post('/ubicacion/registrar', 'UbicacionController@store'); // para registrar en la bd
        Route::put('/ubicacion/actualizar', 'UbicacionController@update'); // para actualizar en la bd
        Route::put('/ubicacion/desactivar', 'UbicacionController@desactivar'); // para desactivar en la bd
        Route::put('/ubicacion/activar', 'UbicacionController@activar'); // para actualizar en la bd
        Route::get('/ubicacion/seleccion', 'UbicacionController@selectUbicacion'); // para tomar datos de las ubicaciones
        
        //************Rutas de Clente Personas******************/
        Route::get('/cliente/codigo', 'ClienteController@codigoscli'); // para tomar el ultimo registro datos de la bd
        Route::get('/cliente', 'ClienteController@index'); // para tomar datos de la bd
        Route::post('/cliente/registrar', 'ClienteController@store'); // para registrar en la bd
        Route::put('/cliente/actualizar', 'ClienteController@update'); // para actualizar en la bd
        Route::get('cliente/selectCliente', 'ClienteController@selectCliente'); //para cargar el select de Trabajo con clientes
        Route::get('cliente/selectClienteEm', 'ClienteController@selectClienteEm'); //para cargar el select de Trabajo con clientes

                
        //************Rutas de Cliente Empresa******************/
        Route::get('/cliente/codigo', 'ClienteController@codigoscli'); // para tomar el ultimo registro datos de la bd
        Route::get('/clienteempresa', 'ClienteController@indexEm'); // para tomar datos de la bd
        Route::post('/cliente/registrar', 'ClienteController@store'); // para registrar en la bd
        Route::put('/cliente/actualizar', 'ClienteController@update'); // para actualizar en la bd
        Route::get('cliente/selectCliente', 'ClienteController@selectCliente'); //para cargar el select de Trabajo con clientes
        Route::get('cliente/selectClienteEm', 'ClienteController@selectClienteEm'); //para cargar el select de Trabajo con clientes
        
        //************Rutas de Roles******************/
        Route::get('/rol', 'RolController@index'); // para tomar datos de la bd
        Route::get('/rol/selectrol', 'RolController@selectRol');
        // Route::put('/cliente/actualizar', 'ClienteController@update'); // para actualizar en la bd
        
        //************Rutas de Usuarios******************/
        Route::get('/user', 'UserController@index'); // para tomar datos de la bd
        Route::post('/user/registrar', 'UserController@store'); // para registrar en la bd
        Route::put('/user/actualizar', 'UserController@update'); // para actualizar en la bd
        Route::put('/user/desactivar', 'UserController@desactivar'); // para desactivar en la bd
        Route::put('/user/activar', 'UserController@activar'); // para actualizar en la bd

         //**Rutas de la ordenes de trabajo */
        Route::get('/ordentra/codigo', 'OrdentrabajoController@codigostra'); // para tomar el ultimo registro datos de la bd
        Route::get('/ordentra', 'OrdentrabajoController@index'); // para tomar datos de la bd
        Route::get('/ordentra/egresados', 'OrdentrabajoController@egresados'); // para tomar todos los registros egresados
        Route::post('/ordentra/registrar', 'OrdentrabajoController@store'); // para tomar datos de la bd
        Route::get('/ordentra/desactivar', 'OrdentrabajoController@desactivar'); // para tomar datos de la bd
        Route::get('/ordentra/obtenerCabeceraEgre', 'OrdentrabajoController@obtenerCabeceraEgresado'); // para tomar datos de la bd ordentra/obtenerCabeceraEgre
        Route::get('/ordentra/obtenerCabecera', 'OrdentrabajoController@obtenerCabecera');
        Route::get('/ordentra/obtenerDetalles', 'OrdentrabajoController@obtenerDetalles'); // para tomar datos de la bd
        Route::get('/ordentra/obtenerDetallesTrabajo', 'OrdentrabajoController@obtenerDetallesTrabajo'); // para tomar datos de la bd
        Route::get('/ordentra/pdf/{id}', 'OrdentrabajoController@pdf')->name('orden_pdf'); // para tomar datos de la bd
        Route::get('/ordentra/pdfnoprecio/{id}', 'OrdentrabajoController@pdfnoprecio')->name('orden_pdfnoprecio'); // para tomar datos de la bd
        Route::get('/ordentra/egresado/pdf/{id}/{id2}', 'OrdentrabajoController@pdfegre')->name('orden_pdf'); // para tomar datos de la bd
        Route::post('/ordentra/egresar', 'OrdentrabajoController@egresar'); // para tomar datos de la bd
        Route::put('/ordentra/costo', 'OrdentrabajoController@costos'); // para tomar datos de la bd

         //Despachos Tipo Empresa
        Route::get('/ordentra/empresa', 'OrdentrabajoController@indexEm'); // para tomar datos de la bd
        Route::get('/ordentra/empresadespa', 'OrdentrabajoController@ordenDespa'); // para tomar datos de la bd
         //

        //Despachos Tipo Paricular
        //  Route::get('/ordentra/empresa', 'OrdentrabajoController@indexEm'); // para tomar datos de la bd
        Route::get('/ordentra/clientedespa', 'OrdentrabajoController@ordenDespaCli'); // para tomar datos de la bd
         //

        //Resumenes de Trabajo
        Route::get('/codigo/estado', 'InventarioController@codigosresu'); // para tomar el ultimo registro datos de la bd
        Route::get('/resumen', 'InventarioController@resumen'); // para tomar datos de la bd
        Route::get('/resumen/pdf/{id}/{ini}/{fin}/{folio}','InventarioController@pdfResumen'); // para tomar datos de la bd
        Route::get('/estadoregistrados','InventarioController@verificar'); // para tomar datos de la bd
        Route::post('/resumen/registrar', 'InventarioController@store'); // para tomar datos de la bd
        Route::get('/estados', 'InventarioController@estados'); // para tomar datos de la bd
        Route::put('/estado/desactivar', 'InventarioController@desactivar'); // para tomar datos de la bd
        // Route::get('/ordentra/egresado/pdf/{id}/{id2}', 'OrdentrabajoController@pdfegre')->name('orden_pdf'); // para tomar datos de la bd
        //

        // Rutas de las guias de Despachos Empresas
        Route::get('/despacho/codigodes', 'DespachoController@codigosdes'); // para tomar el ultimo registro datos de la bd
        Route::get('/despacho', 'DespachoController@index'); // para tomar datos de la bd
        Route::post('/despacho/registrar', 'DespachoController@store'); // para tomar datos de la bd
        Route::get('/despacho/obtenerDetalles', 'DespachoController@obtenerDetalles'); // para tomar datos de la bd
        Route::get('/despacho/obtenerDetallesEmp', 'DespachoController@obtenerDetallesEmpresa'); // para tomar datos de la bd
        Route::get('/despacho/obtenerCabecera', 'DespachoController@obtenerCabecera'); // para tomar datos de la bd
        Route::get('/despacho/obtenerCabeDeta', 'DespachoController@obtenerCabeDetalles'); // para tomar datos de la bd
        Route::get('/despacho/obtenerCabeDetaEm', 'DespachoController@obtenerCabeDetallesEmpresa'); // para tomar datos de la bd
        Route::put('/despacho/despaorden', 'DespachoController@update'); // para tomar datos de la bd
        Route::get('/despacho/pdf/{id}', 'DespachoController@pdf')->name('orden_pdf'); // para tomar datos de la bd
        // 
        // Rutas de las guias de Despachos Clientes
        Route::get('/despacho/cli', 'DespachoController@indexCli'); // para tomar datos de la bd
        
        // 

        
        //**Rutas de Caja**/
         Route::get('/caja', 'CajaController@index'); // para tomar datos de la bd
         Route::get('/caja/despachado', 'CajaController@despachado'); // para tomar datos de la bd
         Route::get('/caja/pagos', 'CajaController@todosPago'); // para tomar datos de la bd
        //  Route::get('/caja/despachado', 'CajaController@despachado'); // para tomar datos de la bd
        
         
        //**Rutas de Inventario**/
        Route::get('/inventario', 'InventarioController@index'); // para tomar datos de la bd
        Route::get('/inventarioEm', 'InventarioController@indexEm'); // para tomar datos de la bd
        Route::get('/inventario/pdf', 'InventarioController@pdf'); // para tomar datos de la bd
        Route::get('/inventario/exportar', 'InventarioController@export'); // Excel
        Route::get('/inventarioEm/exportar', 'InventarioController@exportEm'); // Excel
    
        
        //**Rutas de Estado de Pago**/
        Route::get('/pago', 'InventarioController@pago'); // para tomar datos de la bd
        Route::get('/estadopago/exportar', 'InventarioController@exportpago'); // Excel
        // Route::get('/inventario/pdf', 'InventarioController@pdf'); // para tomar datos de la bd
        // Route::get('/inventario/exportar', 'InventarioController@export'); // Excel
           
        //**Rutas de Movimiento de prendas**/
        Route::get('/movimiento', 'MovimientoController@index'); // para tomar datos de la bd
        Route::put('/movimiento/actualizar', 'MovimientoController@update'); // para actualizar en la bd
        Route::get('/movimiento/estados', 'MovimientoController@estados'); // para tomar datos de la bd

        Route::get('/caja/despachado', 'CajaController@despachado'); // para tomar datos de la bd
        Route::get('/caja/pagos', 'CajaController@todosPago'); // para tomar datos de la bd
 
    });

    Route::group(['middleware' => ['Recepcionista']], function () {
        //Todas las rutas a las que puede acceder el Recepcionista

        //************Rutas de Personas******************/
        Route::get('/cliente/codigo', 'ClienteController@codigoscli'); // para tomar el ultimo registro datos de la bd
        Route::get('/cliente', 'ClienteController@index'); // para tomar datos de la bd
        Route::post('/cliente/registrar', 'ClienteController@store'); // para registrar en la bd
        Route::put('/cliente/actualizar', 'ClienteController@update'); // para actualizar en la bd
        Route::get('cliente/selectCliente', 'ClienteController@selectCliente'); //para cargar el select de Trabajo con clientes
        Route::get('cliente/selectClienteEm', 'ClienteController@selectClienteEm'); //para cargar el select de Trabajo con clientes

        //************Rutas de Prendas******************/
        Route::get('/prenda', 'PrendaController@index'); // para tomar datos de la bd
        Route::post('/prenda/registrar', 'PrendaController@store'); // para registrar en la bd
        Route::put('/prenda/actualizar', 'PrendaController@update'); // para actualizar en la bd
        Route::put('/prenda/desactivar', 'PrendaController@desactivar'); // para desactivar en la bd
        Route::put('/prenda/activar', 'PrendaController@activar'); // para actualizar en la bd
        Route::get('/prenda/buscarPrenda', 'PrendaController@buscarPrenda'); //para buscar la prendas desde el input de orden trabajo
        Route::get('/prenda/listarPrenda', 'PrendaController@listarPrenda'); //para listar la prendas desde el input de orden trabajo
        Route::get('/tipotrabajo/listarTipotrabajo', 'TipotrabajoController@listarTipotrabajo'); //para listar la prendas desde el input de orden trabajo

        
         //**Rutas de la ordenes de trabajo */
         Route::get('/ordentra/codigo', 'OrdentrabajoController@codigostra'); // para tomar el ultimo registro datos de la bd
         Route::get('/ordentra', 'OrdentrabajoController@index'); // para tomar datos de la bd
         Route::get('/ordentra/egresados', 'OrdentrabajoController@egresados'); // para tomar todos los registros egresados
         Route::post('/ordentra/registrar', 'OrdentrabajoController@store'); // para tomar datos de la bd
         Route::get('/ordentra/desactivar', 'OrdentrabajoController@desactivar'); // para tomar datos de la bd
         Route::get('/ordentra/obtenerCabecera', 'OrdentrabajoController@obtenerCabecera'); // para tomar datos de la bd
         Route::get('/ordentra/obtenerDetalles', 'OrdentrabajoController@obtenerDetalles'); // para tomar datos de la bd
         Route::get('/ordentra/obtenerDetallesEmpresas', 'OrdentrabajoController@obtenerDetallesEmpresa'); // para tomar datos de la bd
         Route::get('/ordentra/obtenerDetallesTrabajo', 'OrdentrabajoController@obtenerDetallesTrabajo'); // para tomar datos de la bd
         Route::get('/ordentra/pdf/{id}', 'OrdentrabajoController@pdf')->name('orden_pdf'); // para tomar datos de la bd
         Route::get('/ordentraempresa/pdf/{id}', 'OrdentrabajoController@pdfEmpresa')->name('orden_pdf'); // para PDF Empresa
         Route::get('/ordentra/egresado/pdf/{id}/{id2}', 'OrdentrabajoController@pdfegre')->name('orden_pdf'); // para tomar datos de la bd
         Route::post('/ordentra/egresar', 'OrdentrabajoController@egresar'); // para tomar datos de la bd
         Route::put('/ordenempresa/fechaeditada', 'OrdentrabajoController@editarFecha'); // para actualizar en la bd
         
        //**Rutas de Caja**/
         Route::get('/caja', 'CajaController@index'); // para tomar datos de la bd
         Route::get('/caja/despachado', 'CajaController@despachado'); // para tomar datos de la bd
         Route::get('/caja/pagos', 'CajaController@todosPago'); // para tomar datos de la bd

            //**Rutas de Inventario**/
        Route::get('/inventario', 'InventarioController@index'); // para tomar datos de la bd
        Route::get('/inventario/pdf', 'InventarioController@pdf'); // para tomar datos de la bd
        Route::get('/caja/despachado', 'CajaController@despachado'); // para tomar datos de la bd
        Route::get('/caja/pagos', 'CajaController@todosPago'); // para tomar datos de la bd
        Route::get('/caja/despachado', 'CajaController@despachado'); // para tomar datos de la bd
        Route::get('/caja/pagos', 'CajaController@todosPago'); // para tomar datos de la bd
        
    });

    Route::group(['middleware' => ['Operador']], function () {
        //Todas las rutas a las que puede acceder el Operador

        //************Rutas de Prendas******************/
        Route::get('/prenda', 'PrendaController@index'); // para tomar datos de la bd
        Route::post('/prenda/registrar', 'PrendaController@store'); // para registrar en la bd
        Route::put('/prenda/actualizar', 'PrendaController@update'); // para actualizar en la bd
        Route::put('/prenda/desactivar', 'PrendaController@desactivar'); // para desactivar en la bd
        Route::put('/prenda/activar', 'PrendaController@activar'); // para actualizar en la bd

        //Resumenes de Trabajo
        Route::get('/codigo/estado', 'InventarioController@codigosresu'); // para tomar el ultimo registro datos de la bd
        Route::get('/resumen', 'InventarioController@resumen'); // para tomar datos de la bd
        Route::get('/resumen/pdf/{id}/{ini}/{fin}/{folio}','InventarioController@pdfResumen'); // para tomar datos de la bd
        Route::get('/estadoregistrados','InventarioController@verificar'); // para tomar datos de la bd
        Route::get('/estados', 'InventarioController@estados'); // para tomar datos de la bd

    });  

});

// Route::get('/home', 'HomeController@index')->name('home');
